# IMPORTANT NOTICE

(Nicola Papale 2018)

This project is incomplete, I provide it in the open for posterity only.
It might break or be dependent on specific configurations. I've compiled it
on linux with the DEVKITPRO toolkit installed through the provided perl script.

a preview of what's going on in-game: <http://i.imgur.com/Swe1BvW.gifv>

the lodepng library is bundled with the source code, please read the lodepng.h
and lodepng.c files for further information on their license.

The graphical assets are licensed under the guise of the
[Creative Commons BY-SA](https://creativecommons.org/licenses/by-sa/4.0/). I've
made them myself. While code and building scripts are licensed under the MIT
(a copy is available in the repository). Please, feel free to use them at your
leisure. `kra` files are krita files, `xmp` are GIMP files

The following text is conserved as original, with slight markup alteration to
make it compatible with gitlab's markdown.

# Generic Space Shooter Advance

(Nicola Papale 2016)

This is the sources for my game. There is a lot of issues with the current
build. Notably that the game is not finished yet (namely, there is no gameover
screens or victory screens).

I managed to get the game running with the relatively complexe wave and ennemy
definition system that I wanted it to have. I belive it is already enough proof
of the capacity that was required for this lecture. I myself will finish the
game as a side project later, and those system will help me expand the game
with relative ease.

Again, for the specifications about the .bin file, I compiled them myself from
the ressources_nice files with my own custom converter tool. Those will then be
recognized by devkitpro and converted into header and object files automatically

## BUILDING

This project is only dependent on two things:

* The devkitARM toolchain for building, compiling and linking the source code
* Python to compile the assets into .bin files gba compatible.
* GCC, in order to compile png2gba (last tested with gcc 7.3.1)
* make

### IMPORTANT

Make sure to compile png2gba first. Then compile the whole project.

### OTHER

Cheat codes:
```
4B4643 transforms your ship into a chicken
2B5215 gives you infinite B button use
76031B gives you invulnerability
AEDFXX last 2 numbers is your ammount of lives
```

extra cheat code candidates: Pickup explodes on touch

We need to setup interrupts for DMA transfer of the OAM.
chicken has powerfull shots, but cannot be hit at any rate

- powerup drops are disabled for chicken

your weapon can be modified on two different axes:

- level: increases damage, everytime you get hit, you loose a level.
  If you get hit while you have 0 levels, you loose a life
- types: there is four different weapons: standard, double, helix and laser
  the weapon types drops rarely, each ship starts with a different one
  Blank: helix, Paladin: standard, Spear: double

hit detection:
- bullets are points
- ships are squares
- player hitbox is smaller
- player needs to have some invincibility frames if he gets hit

waves:
- levels are cut into several waves of ennemies, that you have to
  take out or let go outside of the screen to complete and get to the next
  wave.

special ability use:
- each character has a special ability, the ability is refilled by picking
  up battery charges, which drop rate increases the lower your weapon
  level is. Furthermore, the greater the difficulty, the lower the drop
  rate is for higher level weapons.
- Feedback is given to the player, by filling one bar under the ability
  portrait on the top screen bar. When the ability is not avaliable, it is
  grayed out, when it is avaliable, it is colored. This effect can be done
  by modifying the palette values for the ability sprite.

checkpoints:
- if the player gets hit while at weapon level 0, he dies.
- When the player dies, he respawns at different points depending on the
  level of difficulty:
  - easy: last wave
  - normal: three waves back.
  - hard: start of the level

freez frames:
- when the player gets hit, or an ennemy ship is taken down, there is a
   freez frame (aka an inactive frame where nothing happens). This is to
   give a better feedback on stuff

bullets have different colors depending on different factors:
- your bullets:
  - level 0: green
  - level 1: blue
  - level 2: red
  - level 3: yellow
- ennemy bullets: magenta/pink

wave system:
- The level is randomly generated. The game selects random waves in a
  list of waves.
- Each waves lists a sequence of ennemies with the timming, the place and
  speed at which they enter the screen.

ennemies:
- Each ennemy has its own visual, they can have varrying bullet types
  which look corresponds to their behavior.

color handling:
- We will use a reference palette for handling dynamic player ship color,
  dynamic player weapon color, ability cooldown effect.
  Ideally, the ressource is compiled with greyscale colors, and the
  program chooses which color to use depending on which ship is choosen

