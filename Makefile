
#---------------------------------------------------------------------------------
# Clear the implicit built in rules
#---------------------------------------------------------------------------------
.SUFFIXES:
#---------------------------------------------------------------------------------
ifeq ($(strip $(DEVKITARM)),)
$(error "Please set DEVKITARM in your environment. export DEVKITARM=<path to>devkitARM")
endif
include $(DEVKITARM)/gba_rules

#---------------------------------------------------------------------------------
# TARGET is the name of the output, if this ends with _mb generates a multiboot image
# BUILD is the directory where object files & intermediate files will be placed
# SOURCES is a list of directories containing source code
# INCLUDES is a list of directories containing extra header files
#---------------------------------------------------------------------------------
TARGET := GenericSpaceShooterAdvance
BUILD := build
SOURCES	:= source
DATA := resources
SOURCES += $(DATA)
INCLUDES := headers

EMULATOR = mgba

#c flags, we put different flags depending on the target. Note that the include
# flags must ALWAYS be specified at the end
CFLAGS	:= -Wall -Wextra -Wno-missing-braces -mcpu=arm7tdmi -mtune=arm7tdmi \
     -ffast-math $(ARCH)

debug: CFLAGS += -O0 -ggdb
release: CFLAGS += -O2 -fomit-frame-pointer
CFLAGS += $(INCLUDE)
ASFLAGS := $(ARCH)
LDFLAGS = $(ARCH)

#-----------------------------------------------------------
# path to tools - this can be deleted if you set the path in windows
export PATH:=$(DEVKITARM)/bin:$(PATH)

#-----------------------------------------------------------
# any extra libraries we wish to link with the project
LIBS := -lgba

#------------------------------------------------------------
# list of directories containing libraries, this must be the top level containing
# include and lib
LIBDIRS := $(LIBGBA)

#-------------------------------------------------------------
# no real need to edit anything past this point unless you need to add additional
# rules for different file extensions
ifneq ($(BUILD),$(notdir $(CURDIR)))

export OUTPUT := $(CURDIR)/$(TARGET)

export VPATH := $(foreach dir,$(SOURCES),$(CURDIR)/$(dir))
export DEPSDIR := $(CURDIR)/$(BUILD)

#--------------------------------------------------------------
# automatically build a list of object files for our project
CFILES := $(foreach dir,$(SOURCES),$(notdir $(wildcard $(dir)/*.c)))
CPPFILES := $(foreach dir,$(SOURCES),$(notdir $(wildcard $(dir)/*.cpp)))
SFILES  := $(foreach dir,$(SOURCES),$(notdir $(wildcard $(dir)/*.s)))
BINFILES := $(foreach dir,$(SOURCES),$(notdir $(wildcard $(dir)/*.bin)))


#--------------------------------------------------------------
# use CXX for linking C++ projects, CC for standard C
ifeq ($(strip $(CPPFILES)),)
	export LD:=$(CC)
else
	export LD:=$(CXX)
endif
export OFILES := $(BINFILES:.bin=.o) $(CPPFILES:.cpp=.o) $(CFILES:.c=.o) $(SFILES:.s=.o)
export INCLUDE := $(foreach dir,$(INCLUDES),-I$(CURDIR)/$(dir)) \
    $(foreach dir,$(LIBDIRS),-I$(dir)/include) \
    -I$(CURDIR)/$(BUILD)
export LIBPATHS	:= $(foreach dir,$(LIBDIRS),-L$(dir)/lib)

#---------------------------------------------------------------
# Actual targets, compiles the game
.PHONY: $(BUILD) clean run debug make_resources

$(BUILD): make_res/resCompile.txt make_resources
	@[ -d $@ ] || mkdir -p $@
	@make --no-print-directory -C $(BUILD) -f $(CURDIR)/Makefile

make_resources:
	@[ -d $(DATA)] || mkdir -p $(DATA)
	@python $(CURDIR)/make_res/compileParser.py $(CURDIR) \
	make_res/resCompile.txt

clean:
	@echo cleaning behind me
	@rm -fr $(DATA)/*
	@rm -fr $(BUILD) $(TARGET).*

run: $(BUILD)
	@$(EMULATOR) -l 511 $(OUTPUT).gba 2>&1 1>/dev/null &

debug: $(BUILD) $(OUTPUT).elf $(OUTPUT).gba
	@$(EMULATOR) -g $(OUTPUT).gba 2>&1 1>nohup.out &
	@sleep 1; $(DEVKITARM)/bin/arm-none-eabi-gdb $(OUTPUT).elf -ex 'target remote localhost:2345'

#----------------------------------------------------------------
else

DEPENDS := $(OFILES:.o=.d)

#----------------------------------------------------------------
# main targets
#----------------------------------------------------------------
$(OUTPUT).gba : $(OUTPUT).elf

$(OUTPUT).elf : $(OFILES) $(LIBGBA)/lib/libgba.a

%.o : %.bin
	@echo $(notdir $<) "---->" $(@)
	bin2s $< | $(AS) -o $(@)
	echo "extern const u8" `(echo $(<F) | sed -e 's/^\([0-9]\)/_\1/' | tr . _)`"_end[];" > `(echo $(<F) | tr . _)`.h
	echo "extern const u8" `(echo $(<F) | sed -e 's/^\([0-9]\)/_\1/' | tr . _)`"[];" >> `(echo $(<F) | tr . _)`.h
	echo "extern const u32" `(echo $(<F) | sed -e 's/^\([0-9]\)/_\1/' | tr . _)`_size";" >> `(echo $(<F) | tr . _)`.h

-include $(DEPENDS)


#---------------------------------------------------------------
endif
#---------------------------------------------------------------
