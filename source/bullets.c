#include "bullets.h"

#include "exit_debug.h"
#include "sprite_handler.h"
#include "compact_array.h"

#include "bulls_til_bin.h"

static ActiveBullet* onscreenBullets; //onscreen bullets list
static u8 bltCount; //Number of bullets on screen
static u8 bSpritesLoc;

void addBullet(ActiveBullet* newBu)
// Writes into oamClone bullet data, and adds the bullet to the bullet list
{
	if (newBu->xpos > (240 << 8) || newBu->xpos < 0
			|| newBu->ypos < 0 || newBu->ypos > (160 << 8)) {
		return; //don't do anything if attempting to place a bullet outside
		//of the screen
	}

	extern u8 bltCount;
	extern ActiveBullet* onscreenBullets;
	extern u8 bSpritesLoc;

	int sindex ; getSindex(sindex);
	newBu->sindex = sindex;

	if (bltCount >= MAX_BULLET) {
		blueScreen("Too many bullets!");
	}
	insertCompArray(onscreenBullets,bltCount,*newBu);
	//vv and updates sprite memory
	oamClone[sindex].xcoord = (newBu->xpos >> 8) - 4;
	oamClone[sindex].ycoord = (newBu->ypos >> 8) - 4;
	oamClone[sindex].shape = ATR_SQUARE;
	oamClone[sindex].spriteSize = ATR_SIZE8;
	oamClone[sindex].spriteLoc = newBu->bulletID * 2 + bSpritesLoc;
}


void bulletHandler()
//Handles bullet movement, bullet display, and out of screen bullets
{
	extern ActiveBullet* onscreenBullets;
	extern u8 bltCount;

	for (int i = bltCount; i != 0; i--) {
		ActiveBullet* curBullet = onscreenBullets + i-1;
		int cbhspeed = curBullet->hspeed;
		int cbvspeed = curBullet->vspeed;
		int cbxpos = (curBullet->xpos += cbhspeed) >> 8;
		int cbypos = (curBullet->ypos += cbvspeed) >> 8;

		if (abs(cbhspeed) + abs(cbvspeed) < 2
		  || cbxpos < 0   //kills ^^ slow bullets and
		  || cbxpos > 240 // bullets out of screen
		  || cbypos < 0
		  || cbypos > 160
		) {
			killBullet(i-1);
		} else {
			u32 sindex = curBullet->sindex;
			oamClone[sindex].xcoord = cbxpos - 4;
			oamClone[sindex].ycoord = cbypos - 4;
		}
	}
}


void killBullet(int bID)
// removes bullet in the onscreenBullets list at index bID
// and adds back the slot to the free slot stack
{
	extern u8 bltCount;
	extern ActiveBullet* onscreenBullets;

	oamClone[onscreenBullets[bID].sindex].spriteLoc = 0;
	giveSindex(onscreenBullets[bID].sindex);
	extractCompArray(onscreenBullets,bltCount,bID);
}


void initBullets(ActiveBullet actvBlist[])
{
	extern ActiveBullet* onscreenBullets;
	extern u8 bltCount;
	extern u8 bSpritesLoc;

	bSpritesLoc = allocSprite(bulls_til_bin, BULLET_COUNT) * 2;
	onscreenBullets = actvBlist;
	bltCount = 0;
}


int actvBulletCount()
//Used for bullet loop handling outside of this file
{
	extern u8 bltCount;
	return bltCount;
}
