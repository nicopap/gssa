#include "pickups.h"

#include "sprite_handler.h"
#include "compact_array.h"
#include "exit_debug.h"

#include "powerups_til_bin.h"

#define PICKUP_TYPE_COUNT 8
#define PICKUP_SPEED 59

static ActivePickup* osPu; // Array of on screen pick-ups
static u8 puCount;
static u8 spBaseLoc;

static void killPickup(int);

void initPickup(GameState* gs, ActivePickup puArray[])
// oamOffset is the offset in the oamClone at which the pickups sprites
// should be drawn.
// GameState* gs is used to determine what algorithm to use in order to spawn
// the pickups (if chicken -> only powerup, and maybe other stuff in the future)
{
	extern ActivePickup* osPu;
	extern u8 spBaseLoc;
	extern u8 puCount;

	puCount = 0;
	osPu = puArray;
	spBaseLoc = allocSprite(powerups_til_bin, 7) * 2;

	for (int i = MAX_PICKUP; i != 0; i--) {
		osPu[i-1].type = pt_null;
	}
}


void addPickup(enum pickupType pt, int xpos, int ypos)
// Adds pickup of type pt at location (xpos,ypos)
{
	if (xpos < 0 || xpos > (240<<8) || ypos < 0 || ypos > (160<<8))
		return; //Do nothing if attempt to spawn outside of screen boudaries
	extern ActivePickup* osPu;
	extern u8 puCount;
	extern u8 spBaseLoc;

	int sindex; getSindex(sindex);

	oamClone[sindex].xcoord = (xpos >> 8) - 4;
	oamClone[sindex].ycoord = (ypos >> 8) - 4;
	oamClone[sindex].spriteLoc = spBaseLoc + ((u32) pt) * 2 - 2;
	// ^ computes sprite based on specific pickup ^
	ActivePickup newPu;
	newPu.type = pt;
	newPu.xpos = xpos;
	newPu.ypos = ypos;
	newPu.sindex = sindex;
	insertCompArray(osPu,puCount,newPu);
}


void addRandPickup(int xpos, int ypos)
// !!TODO:!! Prevent spawning weapon level ups in case the weapon level
// is already at max
// Prevent spawning weapon of type equals to the one already equipped by player
{
	#define PU_WGHT_weaponUp   55
	#define PU_WGHT_stdWp	  15
	#define PU_WGHT_doubleWp   15
	#define PU_WGHT_chargeWp   0
	#define PU_WGHT_momentumWp 15
	#define PU_WGHT_lifeUp	 0
	#define PU_WGHT_fillAbil   0
	#define PWG(TYPE) PU_WGHT_ ## TYPE

	#define TOTAL_WGHT 100

	int pickupChoice = rand() % TOTAL_WGHT;
	enum pickupType newPut;
	//For best efficiency, sort those from greatest likelyness to lowest
	//Logic block accomplishing weight calculation on whole numbers
	//The likelyness of a pickups should be equal to:
	// PU_WGHT_pickup / TOTAL_WGHT | the sum of all PU_WGHT_ must be TOTAL_WGHT
	if (pickupChoice < PWG(weaponUp))
		newPut = pt_weaponUp;
	else if ((pickupChoice -= PWG(weaponUp)) < PWG(stdWp))
		newPut = pt_stdWp;
	else if ((pickupChoice -= PWG(stdWp)) < PWG(doubleWp))
		newPut = pt_doubleWp;
	else if ((pickupChoice -= PWG(doubleWp)) < PWG(chargeWp))
		newPut = pt_chargeWp;
	else if ((pickupChoice -= PWG(chargeWp)) < PWG(momentumWp))
		newPut = pt_momentumWp;
	else if ((pickupChoice -= PWG(momentumWp)) < PWG(lifeUp))
		newPut = pt_lifeUp;
	else if ((pickupChoice -= PWG(lifeUp)) < PWG(fillAbil))
		newPut = pt_fillAbil;
	else
		blueScreen("RAN OUT OF LIKELYNESS in pickups.c::addRandPickup()\n"
				   "probably, the total weight of pickups is lower than 100");

	addPickup(newPut, xpos, ypos);
}


void collectPickup(ActivePickup* ap, PlayerShip* ps)
{
	extern ActivePickup* osPu;

	switch (ap->type) {
	case pt_fillAbil:
	label_fillAbil:
		ps->abilAval += 1; // THIS NEEDS ADJUSTEMENT AFTER IMPLEMENTATION OF
		//ABILITY
		break;
	case pt_weaponUp: //Turns into pt_fillAbil in case weapon lvl is already max
	label_weaponUp:
		if (ps->curMod == wm_up3)
			goto label_fillAbil;
		else
			weaponUp(ps);
		break;
	case pt_stdWp:
		if (ps->curWeapon == w_standard)
			goto label_weaponUp;
		else
			ps->curWeapon = w_standard;
		break;
	case pt_momentumWp:
		if (ps->curWeapon == w_momentum)
			goto label_weaponUp;
		else
			ps->curWeapon = w_momentum;
		break;
	case pt_doubleWp:
		if (ps->curWeapon == w_double)
			goto label_weaponUp;
		else
			ps->curWeapon = w_double;
		break;
	case pt_chargeWp:
		if (ps->curWeapon == w_charge)
			goto label_weaponUp;
		else
			ps->curWeapon = w_charge;
		break;
	case pt_lifeUp:
		ps->remainingLives += 1;
		break;
	default:
		break;
	}
	killPickup(ap - osPu); // ap-osPu = index of active pickup being killed
}


void pickupHandler()
//Checks if pickups is not out of screen and moves pickups
{
	extern ActivePickup* osPu;
	extern u8 puCount;

	for (int i = puCount; i != 0; i--) {
		int newXpos = osPu[i-1].xpos - PICKUP_SPEED;
		if (newXpos < 0) {// Is out of screen
			killPickup(i-1);
		} else {// Is not out of screen
			osPu[i-1].xpos = newXpos;
			oamClone[osPu[i-1].sindex].xcoord = newXpos >> 8;
		}
	}
}


int actvPickupCount()
{
	extern u8 puCount; return (int) puCount;
}


static void killPickup(int puID)
{
	extern ActivePickup* osPu;
	extern u8 puCount;

	oamClone[osPu[puID].sindex].spriteLoc = 0;
	giveSindex(osPu[puID].sindex);
	extractCompArray(osPu,puCount,puID);
}
