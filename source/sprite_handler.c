#include <stdbool.h>

#include "sprite_handler.h"
#include "exit_debug.h"

struct plage {
	u8 x; //index position
	u8 l; //length of the plage
};

signed char sindexStack[128] = {
	  0,   1,   2,   3,   4,   5,   6,   7,   8,   9,  10,  11,  12,  13,  14,
	 15,  16,  17,  18,  19,  20,  21,  22,  23,  24,  25,  26,  27,  28,  29,
	 30,  31,  32,  33,  34,  35,  36,  37,  38,  39,  40,  41,  42,  43,  44,
	 45,  46,  47,  48,  49,  50,  51,  52,  53,  54,  55,  56,  57,  58,  59,
	 60,  61,  62,  63,  64,  65,  66,  67,  68,  69,  70,  71,  72,  73,  74,
	 75,  76,  77,  78,  79,  80,  81,  82,  83,  84,  85,  86,  87,  88,  89,
	 90,  91,  92,  93,  94,  95,  96,  97,  98,  99, 100, 101, 102, 103, 104,
	105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119,
	120, 121, 122, 123, 124, 125, 126, 127
};
short sindexStart = 0;

static  struct plage plageAry[64];
static  int maxPlage;

static void mergePlages(int indx1, int indx2);
static void loadSprite(const u8* originalSprite, int size, int destIndx);


void initSprites()
{
	extern struct plage plageAry[];
	extern int maxPlage;

	plageAry[0].x = 1; plageAry[0].l = 255;
	maxPlage = 0;
}


int allocSprite(const u8* src, int size)
{
	extern struct plage plageAry[];
	extern int maxPlage;
	int i = 0;
	do {
		struct plage candidate = plageAry[i];
		if (candidate.l >= size) {
			break;
		}
	} while (++i <= maxPlage);
	if (i > maxPlage) {
	  blueScreen("During allocation of sprite, went out of bound of plageAry.");
	}
	struct plage* toModify = plageAry + i;
	int position = toModify->x;
	toModify->l -= size;
	toModify->x += size;

	loadSprite(src, size, position);
	return position;
}


void changeSprite(int dst, const u8* src, int size)
{
	loadSprite(src, size, dst);
}


void freeSprite(int indx, int size)
{
	extern struct plage plageAry[];
	extern int maxPlage;

	int i = maxPlage + 1;
	bool notFound = true;
	{ u8 upper = indx + size; do { //We try to find an existing plage to extend
	  //before adding one
		struct plage curPlg = plageAry[--i];
		if (upper == curPlg.x) {
			plageAry[i].x = (u8) indx;
			plageAry[i].l += size;
			notFound = false;
		} else if (curPlg.x + curPlg.l == indx) {
			plageAry[i].l += size;
			notFound = false;
		}
	} while (i != 0 && notFound); }

	if (notFound) {//We didn't find any plage to extend, we create our own
		struct plage newPlage = {(u8)indx, (u8)size};
		plageAry[++maxPlage] = newPlage;
		if (maxPlage > 64)
			blueScreen("Too many plage indexes! (abnormal)");
	} else {//We need to check to make sure we cannot merge with another plage
		int toMerge = i;
		u8 mergeX = plageAry[i].x;
		u8 mergeUpper = mergeX + plageAry[i].l;

		i = maxPlage + 1; //For clarity sake.
		do {
			struct plage curPlg = plageAry[--i];
			if (mergeUpper == curPlg.x) {
				mergePlages(toMerge, i);
				break; //Mathematically impossible to have more than 1 bording
				//plage, so we stop the loop.
			} else if (curPlg.x + curPlg.l == mergeX) {
				mergePlages(i, toMerge);
				break;
			}
		} while (i != 0);
	}
}

static void mergePlages(int indx1, int indx2)
//Merges plages indx1 and indx2. Expecting indx1 to be the lower part and indx2
//the higher part.
// !!! BORDER EFFECTS !!! (adjustes maxPlage and plageAry as necessary)
{
	extern struct plage plageAry[];
	extern int maxPlage;

	if (indx1 == maxPlage) {//We want to store the plage in indx2
		plageAry[indx2].x = plageAry[indx1].x;
		plageAry[indx2].l += plageAry[indx1].l;
	} else if (indx2 == maxPlage) {//We want to store in indx1
		plageAry[indx1].l += plageAry[indx2].l;
	} else {//We need to replace indx2 by content of last index
		plageAry[indx1].l += plageAry[indx2].l;
		plageAry[indx2] = plageAry[maxPlage];
	}
	maxPlage -= 1;
}


static void loadSprite(const u8* src, int size, int dst)
{
	DMAcopy((void*) (SPRITE_DATA + dst * 32),
	        (void*) src,
	        size * 32, //number of memory transfered
	        DMA16NOW);
}
