#include "player.h"
#include "weapons.h"

#include "exit_debug.h" //blueScreen()
#include "sprite_handler.h" //allocSprite()
#include "game_graphics.h" //PLAYER_BULLET_POS

//Sprite arrays
#include "spear_pal_bin.h"
#include "paladin_pal_bin.h"
#include "blank_pal_bin.h"

#include "spear_til_bin.h"
#include "paladin_til_bin.h"
#include "blank_til_bin.h"

#include "pbull_lv0_pal_bin.h"
#include "pbull_lv1_pal_bin.h"
#include "pbull_lv2_pal_bin.h"
#include "pbull_lv3_pal_bin.h"

static int playerSlot; //location in oam of player sprite
static u8 invicheatActive;

static void playerShoot(PlayerShip* ps, u32 currentInput);
inline static void loadBulletPalette(PlayerShip* ps);


void initPlayer(PlayerShip* pship, GameState* gs)
{
	extern int playerSlot;
	extern u8 invicheatActive;

	pship->xpos = 4<<8;
	pship->ypos = 52<<8;
	pship->cooldown = 10; //Might want to change it depending on
	  //ship choosen by player
	pship->abilAval = 0;
	pship->remainingLives = 3;

	#define BG_PALETTE BACKGROUND_PALETTE
	#define FG_PALETTE SPRITE_PALETTE
	#define SHIP_PAL_POS 1

	//Specific build weapons loading
	switch (gs->playerShip) {
	// Player ship starting weapons
	case st_Spear:
		pship->curWeapon = w_double;
		pship->curMod = wm_up2;
		break;
	case st_Blank:
		pship->curWeapon = w_momentum;
		pship->curMod = wm_up1;
		break;
	case st_Paladin:
		pship->curWeapon = w_momentum;
		pship->curMod = wm_up1;
		break;
	}
	if (gs->chickenCheat) {
		pship->curWeapon = w_standard;
		pship->curMod = wm_default;
	}
	//Cheat loading
	if (gs->invulCheat)
		invicheatActive = 1;

	//Graphics loading
	int playerSpriteLoc;
	switch (gs->playerShip) {
	case st_Blank:
		DMAcopy((void*) (BG_PALETTE + SHIP_PAL_POS),
		        (void*) blank_pal_bin, 7, DMA16NOW);
		DMAcopy((void*) (FG_PALETTE + SHIP_PAL_POS),
		        (void*) blank_pal_bin, 7, DMA16NOW);
		playerSpriteLoc = allocSprite(blank_til_bin, 4);
		break;
	default:
	case st_Paladin:
		DMAcopy((void*) (BG_PALETTE + SHIP_PAL_POS),
		        (void*) paladin_pal_bin, 7, DMA16NOW);
		DMAcopy((void*) (FG_PALETTE + SHIP_PAL_POS),
		        (void*) paladin_pal_bin, 7, DMA16NOW);
		playerSpriteLoc = allocSprite(paladin_til_bin, 4);
		break;
	case st_Spear:
		DMAcopy((void*) (BG_PALETTE + SHIP_PAL_POS),
		        (void*) spear_pal_bin, 7, DMA16NOW);
		DMAcopy((void*) (FG_PALETTE + SHIP_PAL_POS),
		        (void*) spear_pal_bin, 7, DMA16NOW);
		playerSpriteLoc = allocSprite(spear_til_bin, 4);
		break;
	}
	loadBulletPalette(pship);
	getSindex(playerSlot);
	oamClone[playerSlot].xcoord = pship->xpos >> 8;
	oamClone[playerSlot].ycoord = pship->ypos >> 8;
	oamClone[playerSlot].colorType = ATR_256COL;
	oamClone[playerSlot].spriteSize = ATR_SIZE16;
	oamClone[playerSlot].shape = ATR_SQUARE;
	oamClone[playerSlot].spriteLoc = playerSpriteLoc * 2;
}


void displayPlayer(PlayerShip* pship)
{
	static int playerSprite; // Initialized at 0

	if (pship->iFrames % 8 == 1) {//Makes the player blink during invincibility
		int temp = playerSprite; //Swap the two locations
		playerSprite = oamClone[playerSlot].spriteLoc;
		oamClone[playerSlot].spriteLoc = temp;

		if (oamClone[playerSlot].spriteSize == ATR_SIZE16)
			oamClone[playerSlot].spriteSize = ATR_SIZE8;
		else
			oamClone[playerSlot].spriteSize = ATR_SIZE16;
	} else if (playerSprite != 0 && pship->iFrames == 0) {
		oamClone[playerSlot].spriteLoc = playerSprite;
		oamClone[playerSlot].spriteSize = ATR_SIZE16;
		playerSprite = 0;
	}
	oamClone[playerSlot].xcoord = pship->xpos >> 8;
	oamClone[playerSlot].ycoord = pship->ypos >> 8;
}


/* ------------------------- *
 * Player handling functions *
 * ------------------------- */

void playerInputHandler(PlayerShip* ps)
//Handles player input
{
	#define RTST_FG(a,b) (((a) | (b)) != (a))
	static u32 oldInput;
	u32 curInput = REG_KEYVIEW;

	if (ps->iFrames != 0) ps->iFrames -= 1;
	if (ps->cooldown != 0) ps->cooldown -= 1;

	if(RTST_FG(curInput,KEY_UP) && ps->ypos > 0 )
		ps->ypos -= PLAYER_SPEED;
	else if(RTST_FG(curInput,KEY_DOWN) && ps->ypos < (144<<8))
		ps->ypos += PLAYER_SPEED;

	if (RTST_FG(curInput, KEY_LEFT) && ps->xpos > 0)
		ps->xpos -= PLAYER_SPEED;
	else if (RTST_FG(curInput, KEY_RIGHT) && ps->xpos < (224<<8))
		ps->xpos += PLAYER_SPEED;

	if (RTST_FG(curInput, KEY_A))
		playerShoot(ps, curInput);
	else if (ps->curWeapon == w_charge)
		playerShoot(ps, curInput);

	if (RTST_FG(curInput, KEY_B) && ps->abilAval == 0)
		playerAbility(ps);
	/*
	if (TST_FG(curInput, KEY_SELECT))
		blueScreen("You pressed select D:\n"
				   "This is some filler text to test out if a newly "
				   "implemented feature ended up working properly.\n"
				   "I just used a newline (\\n) :D. in the long run, as a "
				   "player, you shouldn't see this. Yet paradoxically, it "
				   "indirectly will help out your game experience by a shit "
				   "ton");*/
	if (RTST_FG(curInput, KEY_START) && curInput != oldInput)
		screenPause();
	oldInput = curInput;
}

void playerHit(PlayerShip* ps)
{
	extern u8 invicheatActive;
	if (invicheatActive) return;

	if (ps->iFrames == 0) {
		weaponDown(ps);
		FREEZ_FRAME;
		ps->iFrames = PLAYER_INV_FRAMES;
	}
}

int playerDamage(PlayerShip* ps)
{
	return ((int) ps->curMod) + 3;
}

void playerAbility(PlayerShip* ps)
{
	if (ps->abilAval >= FILLED_ABILITY) {
		FREEZ_FRAME;
		ps->abilAval = 0;
	}
}

void weaponDown(PlayerShip* ps)
{
	int curMod = (int) ps->curMod - 1;
	if (curMod < 0) {
		screenGameOver();
	} else {
		ps->curMod = (enum WeaponMod) curMod;
		loadBulletPalette(ps);
	}
}

void weaponUp(PlayerShip* ps)
{
	int curMod = (int) ps->curMod + 1;
	ps->curMod = (enum WeaponMod) curMod;
	loadBulletPalette(ps);
}

static void playerShoot(PlayerShip* ps, u32 currentInput)
{
	shootWeapon(ps, currentInput);
}

inline static void loadBulletPalette(PlayerShip* ps)
{
	switch (ps->curMod) {
	default:
	case wm_default:
		DMAcopy((void*) (FG_PALETTE + PLAYER_BULLET_POS),
		        (void*) pbull_lv0_pal_bin, 6, DMA16NOW);
		break;
	case wm_up1:
		DMAcopy((void*) (FG_PALETTE + PLAYER_BULLET_POS),
		        (void*) pbull_lv1_pal_bin, 6, DMA16NOW);
		break;
	case wm_up2:
		DMAcopy((void*) (FG_PALETTE + PLAYER_BULLET_POS),
		        (void*) pbull_lv2_pal_bin, 6, DMA16NOW);
		break;
	case wm_up3:
		DMAcopy((void*) (FG_PALETTE + PLAYER_BULLET_POS),
		        (void*) pbull_lv3_pal_bin, 6, DMA16NOW);
		break;
	}
}
