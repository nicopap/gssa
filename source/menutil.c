#include "menutil.h"

void menuGodir(struct menuui* menu, enum direction dir)
{
	if (menu->switchAction == NULL) {
		switch (dir) {
			case DIR_down:
				menu->currentEntry = (menu->currentEntry+1) % menu->entryCount;
				break;
			case DIR_up:
				menu->currentEntry = (menu->currentEntry-1) % menu->entryCount;
				break;
			case DIR_left:
				menu->cancelAction(menu);
				break;
			case DIR_right:
				menu->entries[menu->currentEntry].actionFunction();
				break;
		}
	} else {
		menu->switchAction(menu, dir);
	}
}

void menuHandle(struct video_menuui* vmenu)
{

}
