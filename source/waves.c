#include <stdbool.h>

#include "waves.h"

//several waves of colums with windows that the player must aline with
const WaveEntry easyKamy1[] = {
//  ID, Hspd, Vspd,    X,    Y, holdTime
	 7, -270,    0,  235,    4, 0,
	 6, -270,    0,  239,   32, 0,
	 7, -270,    0,  235,   56, 0,
	 7, -270,    0,  235,   82, 0,
	 7, -270,    0,  235,  108, 0,
	 7, -270,    0,  235,  137, 5,

	 7, -300,    0,  235,    4, 0,
	 7, -300,    0,  235,   28, 0,
	 7, -300,    0,  235,   56, 0,
	 7, -300,    0,  235,   82, 0,
	 7, -300,    0,  235,  137, 4,

	 7, -300,    0,  235,    4, 0,
	 7, -300,    0,  235,   28, 0,
	 7, -300,    0,  235,   56, 0,
	 7, -300,    0,  235,  108, 0,
	 7, -300,    0,  235,  137, 4,

	 7, -300,    0,  235,    4, 0,
	 7, -300,    0,  235,   56, 0,
	 7, -300,    0,  235,   82, 0,
	 7, -300,    0,  235,  108, 0,
	 7, -300,    0,  235,  137, 4,

	 7, -300,    0,  235,    4, 0,
	 6, -300,    0,  239,   32, 0,
	 7, -300,    0,  235,   56, 0,
	 7, -300,    0,  235,   82, 0,
	 7, -300,    0,  235,  108, 0,
	 7, -300,    0,  235,  137, 1,
};

const WaveEntry wavyKamy1[] = {
//  ID, Hspd, Vspd,    X,    Y, holdTime
	 9, -100,  120,  235,   16, 0,
	10, -100,  -80,  235,   52, 0,
	10, -100,   80,  235,   92, 0,
	 9, -100, -120,  235,  128,10,

	 9, -100,  160,  235,   42, 0,
	 9, -100,  160,  235,   60, 0,
	 8, -100, -100,  235,  128, 4,
	 9, -100, -160,  235,  100, 0,
	 9, -100, -160,  235,  122, 0,
	 8, -100,  100,  235,   20, 4,
	 9, -100,  160,  235,   42, 0,
	 9, -100,  160,  235,   60, 0,
	 8, -100, -100,  235,  128, 4,
	 9, -100, -160,  235,  100, 0,
	 9, -100, -160,  235,  122, 0,
	 8, -100,  100,  235,   20, 4,
	 9, -100,  160,  235,   42, 0,
	 9, -100,  160,  235,   60, 0,
	 8, -100, -100,  235,  128, 9,

	 9, -100,   90,  235,   10, 0,
	 9, -100,  -90,  235,   45, 0,
	 9, -100,   90,  235,   80, 0,
	 9, -100,  -90,  235,  115, 0,
	 9, -100,   90,  235,  140, 8,

	 8, -100,  -90,  235,   10, 0,
	 8, -100,   90,  235,   45, 0,
	 8, -100,  -90,  235,   80, 0,
	 8, -100,   90,  235,  115, 0,
	 8, -100,  -90,  235,  140, 8,

	 9, -100,   90,  235,   10, 0,
	 9, -100,  -90,  235,   45, 0,
	 9, -100,   90,  235,   80, 0,
	 9, -100,  -90,  235,  115, 0,
	 9, -100,   90,  235,  140, 8,

	 8, -100,  -90,  235,   10, 0,
	 8, -100,   90,  235,   45, 0,
	 8, -100,  -90,  235,   80, 0,
	 8, -100,   90,  235,  115, 0,
	 8, -100,  -90,  235,  140, 8,
};

const WaveEntry lockerWave[] = {
//   ID, Hspd, Vspd,    X,    Y, holdTime
	 11,    0,    2,  224,  -10, 1,
	 11,    0,    2,  208,  180, 1,
	 11,    0,    2,  192,  -10, 1,
	 11,    0,    2,  176,  180, 1,
	 11,    0,    2,  160,  -10, 1,
	 11,    0,    2,  144,  180, 1,
	 11,    0,    2,  128,  -10, 1,
	 11,    0,    2,  112,  180, 1,
	 11,    0,    2,   96,  -10, 1,
	 11,    0,    2,   80,  180, 1,
	 11,    0,    2,   64,  -10, 0,
};

const WaveEntry rnbwKamiWave[] = {
//   ID, Hspd, Vspd,    X,    Y, holdTime
	 14, -100,    0,  235,   16, 2,
	 15, -100,    0,  235,   32, 2,
	 16, -100,    0,  235,   48, 2,
	 17, -100,    0,  235,   64, 2,
	 18, -100,    0,  235,   80, 2,
	 19, -100,    0,  235,   96, 0,
};

const WaveEntry dodgerWave[] = {
//   ID, Hspd, Vspd,    X,    Y, holdTime
	 12,    0,   10,  208,  -10, 0,
};

const WaveEntry stdWave[] = {
//   ID, Hspd, Vspd,    X,    Y, holdTime
	  1, -180,  -20,  240,  140, 0,
};

const WaveEntry spinnerWave[] = {
//   ID, Hspd, Vspd,   X,    Y, holdTime
	 21, -10,    0,  200,   72, 0,
	 21, -10,   10,  200,   10, 0,
	 21, -10,  -10,  200,  134, 0,
};

const WaveEntry spinnerBossWave[] = {
//   ID, Hspd, Vspd,    X,    Y, holdTime
	 22,   -5,    0,  200,   72, 0,
};

#define ENTRY(name) (sizeof(name)/sizeof(WaveEntry)) ,name
const Wave waveSheet[] = {
	0, NULL,
	ENTRY(easyKamy1), //1
	ENTRY(wavyKamy1), //2
	ENTRY(lockerWave), //3
	ENTRY(dodgerWave), //4
	ENTRY(rnbwKamiWave), //5
	ENTRY(stdWave), // 6
	ENTRY(spinnerWave), //7
	ENTRY(spinnerBossWave), //8
};

/* ---------------------- *
 * Wave utility functions *
 * ---------------------- */

volatile int WAVE_COMPLETION_FLAG;
static Wave* currentWave;
static int curTimer;

void readWave()
// reads current ennemy in wave and spawns it. Then sets up interrupt for next
// ennemy. If the waiting time for the next ennemy is wt_none, then immediately
// spawns it.
{
	extern Wave* currentWave;
	extern int curTimer;
	static int currentSpawn;
	bool noWaiting;
	//loop needed for waves with wt_none waiting time, so several ennemies
	//can be spawned at the same time
	do {
		noWaiting = false;
		const WaveEntry* crntEnnemy = currentWave->enList + currentSpawn;
		// Spawns current ennemy
		ActiveEnnemy newEnnemy = {
			crntEnnemy->ennemyID, 0, 0,
			crntEnnemy->xpos << 8,
			crntEnnemy->ypos << 8,
			crntEnnemy->hspeed,
			crntEnnemy->vspeed,
			ennemySheet[crntEnnemy->ennemyID].startHP,
			0,0,0,0
		}; addEnnemy(&newEnnemy);
		//Increments stuff, checks if we finished this wave
		currentSpawn++;
		if (currentSpawn == currentWave->waveSize) {
			currentSpawn = 0;
			endWave();
			return; //Ends this wave
		}
		// Timer setup for the next ennemy
		REG_TM0D = 0;
		//REG_IE &= ~INT_TIMER1;
		REG_TM0CNT = TIMER_FREQUENCY_64;
		REG_TM1CNT = 0;

		if (crntEnnemy->holdTime == 0) {
			noWaiting = true;
		} else {
			//REG_IE |= INT_TIMER1;
			//REG_TM1D = - (crntEnnemy->holdTime);
			REG_TM1D = 0;
			curTimer = crntEnnemy->holdTime;
		}
		REG_TM1CNT = TIMER_ENABLE /*| TIMER_IRQ_ENABLE*/ | TIMER_OVERFLOW;
		REG_TM0CNT |= TIMER_ENABLE;
	} while (noWaiting);
}


void endWave()
{
	extern volatile int WAVE_COMPLETION_FLAG;
	//REG_IME = 0;
	  WAVE_COMPLETION_FLAG = WCF_OVER;
	  REG_TM0CNT = 0;
	  REG_TM1CNT = 0;
	  REG_TM0D = 0;
	  REG_TM1D = 0;
	//  REG_IE &= ~(INT_TIMER0 | INT_TIMER1);
	//REG_IME = 1;
}


void spawnWave(int ID)
{
	extern const Wave waveSheet[];
	extern Wave* currentWave;
	extern volatile int WAVE_COMPLETION_FLAG;

	currentWave = waveSheet + ID;
	WAVE_COMPLETION_FLAG = WCF_INCOURSE;
	//REG_IME = 0;
	  readWave();
	//REG_IME = 1;
}

void testWaveTime()
// Tests if the current timer is ready to send a new ennemy on screen
{
	extern int curTimer;
	if (curTimer == 0) return;
	u32 currentTimer = REG_TM1D;
	if (currentTimer >= curTimer) {
		readWave();
	}
}


void wave_interHandler()
/* handles interruptions */
{
	REG_IME = 0x00;
	u16 oldInterrupts = REG_IF;
	if (TST_FG(oldInterrupts, INT_VCOUNT))
		vcountHandler();
	else if ((oldInterrupts & (INT_TIMER0 | INT_TIMER1 | INT_TIMER2)) != 0)
		readWave();

	REG_IF = oldInterrupts;
	REG_IME = 1;
}


void setupWAVEhandler()
{
	REG_IME = 0;
	  REG_INTRPT = (u32) wave_interHandler;
	  REG_IE |= (INT_TIMER0 | INT_TIMER1);
	REG_IME = 1;
}


int currentWaveEnnemyCount()
{
	extern Wave* currentWave;
	return currentWave->waveSize;
}
