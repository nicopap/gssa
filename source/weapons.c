#include "weapons.h"

void shootWeapon(PlayerShip* ps, u32 pInpt)
{
	#define STD_B_SPEED 400
	#define RTST_FG(a,b) (((a) | (b)) != (a))

	if (ps->cooldown == 0) {
	switch (ps->curWeapon) {
	default:
	case w_standard: {
		#define STD_COOLDOWN 32
		static int upShot; //Enables alternating between firing sides
		ActiveBullet newBullet = {
			ps->xpos + (15<<8),
			ps->ypos + (((upShot)?10:6)<<8),
			STD_B_SPEED,
			10,
			10, 0, 0
		}; addBullet(&newBullet);
		upShot = (upShot)? 0 : 1;
		ps->cooldown = STD_COOLDOWN;
	} break;
	case w_double: {
		#define WDBL_COOLDOWN 45
		#define WDBL_HSPD 580
		ActiveBullet newBullet1 = {
			ps->xpos + (15<<8),
			ps->ypos + (12<<8),
			WDBL_HSPD,
			5,
			10, 0, 0
		}; addBullet(&newBullet1);
		ActiveBullet newBullet2 = {
			ps->xpos + (15<<8),
			ps->ypos + (4<<8),
			WDBL_HSPD,
			-5,
			10, 0, 0
		}; addBullet(&newBullet2);
		ps->cooldown = WDBL_COOLDOWN;
	} break;
	case w_momentum: {
		#define WMMT_COOLDOWN 20
		#define WMMT_HSPD_ORG 100
		#define WMMT_VSPD_MOD 30
		#define WMMT_HSPD_MOD 80

		i16 vspeed = 0;
		i16 hspeed = WMMT_HSPD_ORG;

		if (RTST_FG(pInpt, KEY_DOWN)) { //vertical movement
			vspeed += WMMT_VSPD_MOD;
		} else if (RTST_FG(pInpt, KEY_UP)) {
			vspeed -= WMMT_VSPD_MOD;
		}

		if (RTST_FG(pInpt, KEY_LEFT)) {
			hspeed -= WMMT_HSPD_MOD;
		} else if (RTST_FG(pInpt, KEY_RIGHT)) {
			hspeed += WMMT_HSPD_MOD;
		}

		ActiveBullet newBullet = {
			ps->xpos + (15<<8),
			ps->ypos + (8<<8),
			hspeed,
			vspeed,
			10, 0, 0
		}; addBullet(&newBullet);
		ps->cooldown = WMMT_COOLDOWN;
	} break;
	case w_charge: {
		//WORKINPROGRESS

	} break;
	} }
}
