@ Draws a frame in memory starting at location loc, with tileset starting at
@ tile presuming the frame is arranged like this: F  =  ¬  |¦  ¦|  |_  =  _|
@ Framing a rectangle of width w and height h. Assumes the tileset is 32 wide.
@ Please w and h must be greater than 2, or bad stuff will happen (seriously)
@ u8drawFrame(u32 loc, u32 tile, u32 w, u32 h);
@ r0 = location
@ r1 = baseTile
@ r2 = width
@ r3 = height

  .ARM
  .ALIGN
  .GLOBAL u8drawFrame

u8drawFrame:
  STMFD sp!,{r4-r7,lr} @ Save register r4 to r7 on stack
  MOV r4, r2 @ r4 = width

  ANDS r7, r0, #1 @if r0 % 2 == 0
    MOVEQ r6, r1
  @else
    LDRNEH r6, [r0, #-1]
    ANDNE r6, r6, #0xFF
    ORRNE r6, r6, r1, LSL #8
    STRNEH r6, [r0, #-1]

  ADD r1, r1, #1 @r1 is new tile to be drawn
  firstLine: @draws top line
    ADD r0, r0, #1
    ANDS r7, r0, #1 @if r0%2 == 0
      MOVEQ r6, r1
    @else
      MOVNE r7, r1, LSL #8
      ORRNE r6, r6, r7
      STRNEH r6, [r0, #-1]

    SUBS r4, r4, #1
    BNE firstLine @if --r4 == 0 break
  @endfirstLine:

  ADD r0, r0, #1
  ADD r1, r1, #1 @switch to next tile

  ANDS r7, r0, #1 @if r0 % 2 == 0
    LDREQH r6, [r0]
    ANDEQ r6, r6, #0xFF00
    ORREQ r6, r6, r1
    STREQH r6, [r0] @Store top right corner
  @else
    MOVNE r7, r1, LSL #8
    ORRNE r6, r6, r7
    STRNEH r6, [r0, #-1]

  ADD r2, r2, #1

  ADD r1, r1, #2 @ switches to following tile
  MOV r4, r3 @ r4 = height
  RSB r5, r2, #32 @ r5 = squares to skip to draw to next line (32 - r2)

  sideLines: @draws left and right lines
    SUB r1, r1, #1 @switch side tile
    ADD r0, r0, r5 @go to next line

    ANDS r7, r0, #1 @if r0 % 2 == 0
      LDREQH r6, [r0]
      ANDEQ r6, r6, #0xFF00
      ORREQ r6, r6, r1
      STREQH r6, [r0]
    @else
      LDRNEH r6, [r0, #-1]
      ANDNE r6, r6, #0xFF
      ORRNE r6, r1, LSL #8
      STRNEH r6, [r0, #-1]

    ADD r0, r0, r2
    ADD r1, r1, #1

    ANDS r7, r0, #1 @if r0 % 2 == 0
      LDREQH r6, [r0]
      ANDEQ r6, r6, #0xFF00
      ORREQ r6, r6, r1
      STREQH r6, [r0]
    @else
      LDRNEH r6, [r0, #-1]
      ANDNE r6, r6, #0xFF
      ORRNE r6, r1, LSL #8
      STRNEH r6, [r0, #-1]

    SUBS r4, r4, #1
    BNE sideLines @if --r4 == 0, break
  @end sideLines

  ADD r1, r1, #1
  ADD r0, r0, r5

  ANDS r7, r0, #1 @if r0 % 2 == 0
    MOVEQ r6, r1
  @else
    LDRNEH r6, [r0, #-1]
    ANDNE r6, r6, #0xFF
    ORRNE r6, r1, LSL #8
    STRNEH r6, [r0, #-1] @draws bottom left corner

  ADD r1, r1, #1
  SUB r4, r2, #1 @ r4 = width

  lastLine: @draws top line
    ADD r0, r0, #1
    ANDS r7, r0, #1 @if r0 % 2 == 0
      MOVEQ r6, r1
    @else
      MOVNE r7, r1, LSL #8
      ORRNE r6, r6, r7
      STRNEH r6, [r0, #-1]
    SUBS r4, r4, #1
    BNE lastLine @if --r4 == 0 break
  @end lastLine

  ADD r0, r0, #1
  ADD r1, r1, #1 @switch to next tile

  ANDS r7, r0, #1 @if r0 % 2 == 0
    LDREQH r6, [r0]
    ANDEQ r6, r6, #0xFF00
    ORREQ r6, r6, r1
    STREQH r6, [r0]
  @else
    MOVNE r7, r1, LSL #8
    ORRNE r6, r6, r7
    STRNEH r6, [r0, #-1]

  LDMFD sp!,{r4-r7,pc} @ Restore registers r4 to r7 and MOV pc, lr
