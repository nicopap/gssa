@ Draws moisaic of size L x H at location loc, with mapwidth as width
@ useful for cleaning tiles
@ void u8drawMos(u8* loc, u8 tile, u32 L, u32 H, u32 mapwidth);
@ r0 = location
@ r1 = tile
@ r2 = length of pic (# of tiles)
@ r3 = height of pic (# of tiles)
@ [sp] = width of map

@ r4 iteration over line length
@ r5 step to next line
@ r6 temporary buffer for neighboor buffer replacement

  .ARM
  .ALIGN
  .GLOBAL u8drawMos

u8drawMos:
  STMFD sp!,{r4-r6, lr} @ Save register r4 to r8 on stack
  LDR r5, [sp, #16] @ r8 = mapwidth

  ANDS r4, r0, #1 @ if (r0 % 2 == 0)
    BEQ testErightE
  @else
    SUBNE r2, r2, #1
    ADDNE r0, r0, #1
    BNE testOrightE
  testErightE:
    ADD r4, r0, r2
    ANDS r4, r4, #1 @ if (r0 % 2 == 0)
      MOVEQ r4, r2
      SUBEQ r5, r5, r2
      BEQ lineEE
    @else
      SUBNE r2, r2, #1
      MOVNE r4, r2 @if last column uneven, we setup differently
      SUBNE r5, r5, r2
      BNE lineEO
  testOrightE:
    ADD r4, r0, r2
    ANDS r4, r4, #1 @ if (r0 % 2 == 0)
      MOVEQ r4, r2
      SUBEQ r5, r5, r2
      BEQ lineOE
    @else
      SUBNE r2, r2, #1 @if last column uneven, we setup differently
      MOVNE r4, r2
      SUBNE r5, r5, r2
      BNE lineOO


  lineEE:@means width of picture is even
    tileEE:
      STRB r1, [r0], #2
      SUBS r4, r4, #2 @if we did not reach end of line of picture
      BNE tileEE
    @we set up the next line, check if we reached end of picture
    MOV r4, r2 @resets line length
    ADD r0, r0, r5 @move pointer to next row on map mem
    SUBS r3, r3, #1 @check if we drew everything
    BNE lineEE @if not, we continue with next row
  B endJob @else we end job


  lineEO:
    tileEO:
      STRB r1, [r0], #2
      SUBS r4, r4, #2 @if we did not reach end of line of picture
      BNE tileEO
    @We finished the line but the last column
    LDRH r6, [r0]       @Loads old value at location of last column
    AND r6, r6, #0xFF00 @and replaces it with half of it with the new
    ORR r6, r6, r1      @value in the first byte.
    STRH r6, [r0], r5   @Plus moves pointer to next row on map mem
    MOV r4, r2
    SUBS r3, r3, #1 @check if we reached last line
    BNE lineEO @if not, we draw the next one
  B endJob


  lineOE:
    LDRH r6, [r0,#-2]      @Loads first column
    AND r6, r6, #0xFF      @with a mask and everything
    ORR r6, r6, r1, LSL #8 @
    STRH r6, [r0,#-2]      @
    tileOE:
      STRB r1, [r0], #2
      SUBS r4, r4, #2 @if we did not reach end of line of picture
      BNE tileOE
    @We finished the line
    MOV r4, r2 @resets line length
    ADD r0, r0, r5 @move pointer to next row on map mem
    SUBS r3, r3, #1 @check if we drew everything
    BNE lineOE
  B endJob @else we end job


  lineOO:@means width of picture is even
    LDRH r6, [r0,#-2]      @Loads first column
    AND r6, r6, #0xFF      @with a mask and everything
    ORR r6, r6, r1, LSL #8 @
    STRH r6, [r0,#-2]      @
    tileOO:
      STRB r1, [r0], #2
      SUBS r4, r4, #2 @if we did not reach end of line of picture
      BNE tileOO
    @We finished the line but the last column
    LDRH r6, [r0]       @Loads old value at location of last column
    AND r6, r6, #0xFF00 @and replaces it with half of it with the new
    ORR r6, r6, r1      @value in the first byte.
    STRH r6, [r0], r5   @Plus moves pointer to next row on map mem
    MOV r4, r2
    SUBS r3, r3, #1 @check if we reached last line
    BNE lineOO @if not, we draw the next one
  B endJob


  endJob:
    LDMFD sp!,{r4-r6, pc} @ Restore registers r4 to r8 and MOV pc, lr
