
#include "exit_debug.h"

#include "menuset_pal_bin.h"
#include "menuset_til_bin.h"

void blueScreen(char* errMsg)
{
	REG_IE = 0;
	REG_DISPCNT = VID_MODE_0 | BG0_ENABLE;

	volatile bgcnt* bg0cnt =  (bgcnt*) 0x4000008; //REG_BG0CNT
	bg0cnt->screenBaseBlock = 14;
	bg0cnt->paletteType = 1;

	DMAcopy((void*)BACKGROUND_PALETTE, (void*)menuset_pal_bin, 16, DMA16NOW);
	DMAcopy((void*)TILE_SHEET, (void*)menuset_til_bin, 3840, DMA16NOW);
	BACKGROUND_PALETTE[0] = RGB(64,64,255);

	int i = -1;
	int xpos = 0; int ypos = 0;
	while (errMsg[++i] != '\0') {
		if (errMsg[i] == '\n') {
			xpos = 0; ypos += 1;
		} else {
			BG_TILEMAP(14)[(xpos++) + ypos*32].xcoord = CHAR_TILE(errMsg[i]);
		}

		if (xpos  == 30) {
			xpos = 0;
			ypos += 1;
		}
	}
	for(;;);
}
