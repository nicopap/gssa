
/* What I want to do:
 *-Several different playable ships, each having a unique B button functionality
 *  and different graphics.
 *-One or more level with a scrolling randomly generated background
 *-Scripted ennemy waves.
 *-About 16 different ennemy ships, with different behavior each (maybe less).
 *-A final boss with a lot of bullets
 *-Different power ups modifying the A button shooting ability of your ship
 *-(?)Life system with checkpoints
 *-Difficulty levels, which functionality is unknown yet.
 *-A pause menu, letting you reset, go to main menu or resume gameplay
 */


/* ship abilities:
 * shipA: B button procs armor reflecting bullets
 * shipB: B button clears the bullets on screen
 * shipC: B button offensive ability
 */

#include "gba_def.h"

#include "gamestate.h" //For variables relative to the current game state
#include "mainmenu.h" // bootMenu()
#include "gameplay.h" // playGame()

GameState GAMESTATE_MEM;

int main()
{
	GameState* gameState = &GAMESTATE_MEM;
	gameState->playerShip = st_Paladin;

	//Here start game
	for(;;) {
		REG_DISPCNT = (VID_MODE_0 | BG0_ENABLE | BG1_ENABLE);
		bootMenu(gameState);
		//Here start gameplay
		playGame(gameState);
	}
	return 0;
}
