#include "ennemies.h"

#include "exit_debug.h"
#include "sprite_handler.h"
#include "compact_array.h"

// Assets
#include "medG1_til_bin.h"
#include "medB1_til_bin.h"
#include "medV1_til_bin.h"
#include "smallG1_til_bin.h"
#include "smallG4_til_bin.h"
#include "smallG2_til_bin.h"
#include "bigG1_til_bin.h"
#include "smallB1_til_bin.h"

void stdAI	(ActiveEnnemy*, PlayerShip*);
void stdWaveAI(ActiveEnnemy*, PlayerShip*);
void spinShootAI(ActiveEnnemy* enm, PlayerShip* ps);
void turretAI (ActiveEnnemy*, PlayerShip*);
void zippyAI  (ActiveEnnemy*, PlayerShip*);
void swirrlyAI(ActiveEnnemy*, PlayerShip*);
void dodgeAI  (ActiveEnnemy*, PlayerShip*);
void lineKamikazeAI(ActiveEnnemy*, PlayerShip*);
void waveKamikazeAI(ActiveEnnemy*, PlayerShip*);
void lockerAI(ActiveEnnemy*, PlayerShip*);
void sneakySpammerAI(ActiveEnnemy*, PlayerShip*);
void smartAI (ActiveEnnemy*, PlayerShip*);
void randomAI(ActiveEnnemy*, PlayerShip*);
void spinnerShooterAI(ActiveEnnemy*, PlayerShip*);

#define DecCP static struct CannonPos
  DecCP medG1Pos[] = {0,4,	0,12};
#undef DecCP

struct Box enmBoxList[] = {
#define BIGG1_BLIST enmBoxList + 0
#define BIGG1_BCOUNT 2
	0,8,32,8,
	8,0,8,32,
};

BoxDes enmBoxDesc[] = {
#define BIGG1_BOX enmBoxDesc + 0
	BIGG1_BLIST, BIGG1_BCOUNT,
};

//static struct CannonPos medV2[] = {0,8};
//static struct CannonPos bigG1Pos[] = {5,5,	5,27};

static struct CannonPos smallB1Pos[] = {4,4};
static CannonDes smallB1Cpos = {smallB1Pos,1};

//ennemy graphic |sprite          | spriteSize|spriteShape|hitbox   | Cannons
#define MEDB1_PROP medB1_til_bin,   ATR_SIZE16, ATR_SQUARE,     NULL, NULL
#define MEDG1_PROP medG1_til_bin,   ATR_SIZE16, ATR_SQUARE,     NULL, NULL
#define MEDV1_PROP medV1_til_bin,   ATR_SIZE16, ATR_SQUARE,     NULL, NULL
#define SMLG1_PROP smallG1_til_bin,  ATR_SIZE8, ATR_SQUARE,     NULL, NULL
#define SMLG2_PROP smallG2_til_bin,  ATR_SIZE8, ATR_SQUARE,     NULL, NULL
#define SMLG4_PROP smallG4_til_bin,  ATR_SIZE8, ATR_SQUARE,     NULL, NULL
#define SMLB1_PROP smallB1_til_bin,  ATR_SIZE8, ATR_SQUARE,     NULL, &smallB1Cpos
#define BIGG1_PROP bigG1_til_bin,   ATR_SIZE32, ATR_SQUARE, BIGG1_BOX, NULL

const Ennemy ennemySheet[] = {
//  sprite	| hp|bullet| ai movment funct | ai shoot funct?
	SMLG2_PROP, 10,   3, &stdAI, //0
	SMLG1_PROP, 10,   3, &stdAI, //1
	MEDB1_PROP, 10,   3, &zippyAI , //2
	SMLG4_PROP, 10,   3, &dodgeAI, //3
	MEDG1_PROP, 10,   3, &waveKamikazeAI, //4
	MEDV1_PROP, 10,   3, &lockerAI, //5
	SMLG2_PROP,  6,   3, &lineKamikazeAI, //6
	MEDB1_PROP, 30,   3, &lineKamikazeAI, //7
	SMLG4_PROP,  9,   3, &waveKamikazeAI, //8
	MEDG1_PROP, 12,   3, &stdWaveAI, //9
	MEDB1_PROP, 24,   5, &stdWaveAI, //10
	SMLG1_PROP,  3,   3, &lockerAI, //11
	MEDV1_PROP,  5,   3, &lockerAI, //12
	SMLG2_PROP,  3,   5, &dodgeAI, //13
	MEDB1_PROP,  5,   5, &dodgeAI, //14
	MEDB1_PROP, 10,   0, &stdAI, //15
	MEDG1_PROP, 10,   1, &stdAI, //16
	MEDV1_PROP, 10,   2, &stdAI, //17
	SMLG1_PROP, 10,   3, &stdAI, //18
	SMLG2_PROP, 10,   4, &stdAI, //19
	SMLG4_PROP, 10,   5, &stdAI, //20
	SMLB1_PROP,  5,   3, &spinShootAI, //21
	BIGG1_PROP,150,   3, &spinnerShooterAI, //22
};


/* ---------------------- *
 * Logic Ennemy functions *
 * ---------------------- */


static void AImodify(ActiveEnnemy*, PlayerShip*);
static void ennemyDisplay(int ennemyLoc);
static void ennemyShoot(ActiveEnnemy*,int hspeed, int vspeed, int bulletID);
static int testInsideBox(int x, int y, const Ennemy* subject);

static ActiveEnnemy* ennemArray;
static u8 enmCount;


void initEnnemies(ActiveEnnemy osEnnemyArray[])
{
	extern ActiveEnnemy* ennemArray;
	extern u8 enmCount;
	ennemArray = osEnnemyArray;
	enmCount = 0;
}

void ennemyHandler(PlayerShip* ps)
{
	extern u8 enmCount;
	extern ActiveEnnemy* ennemArray;

	int pXpos = ps->xpos; int pYpos = ps->ypos;
	for (int i = enmCount; i != 0; i--) {
		ActiveEnnemy* curEnm = ennemArray + i-1;
		if (inHitBox(curEnm, pXpos + (2<<8),  pYpos + (2<<8)) ||
		  inHitBox(curEnm,   pXpos + (14<<8), pYpos + (2<<8)) ||
		  inHitBox(curEnm,   pXpos +  (2<<8), pYpos + (14<<8))||
		  inHitBox(curEnm,   pXpos + (14<<8), pYpos + (14<<8)) ) {
			playerHit(ps);
		}
		ennemyDisplay(i-1);
		AImodify(curEnm, ps);

		int expos = curEnm->xpos >> 8;
		int eypos = curEnm->ypos >> 8;

		if ( expos < -64 ||
		  expos > 256 ||
		  eypos < -100 ||
		  eypos > 320) {
			killEnnemy(i-1);
		}
	}
}

void ennemyHit(ActiveEnnemy* hitEnm, PlayerShip* ps)
// ps contains the information regarding the bullet power, therefore
// we need to know the player in order to hit the Ennemy properly.
// We also need to know which bullet type struke the ennemy
{
	extern ActiveEnnemy* ennemArray;

	//There is alot of stuff that needs ot be done here tghat I cannont
	//do yet.
	int remHP = (hitEnm->hp -= playerDamage(ps));
	if (remHP <= 0)
		killEnnemy(hitEnm - ennemArray); // pointer arithmetic
}

void killEnnemy(int enmIdx)
//Removes the ennemy from the active ennemy array and removes the listing
//of his sprite in the sprite handler And spawns a powerup
{
	extern u8 enmCount;
	extern ActiveEnnemy* ennemArray;
	extern const Ennemy ennemySheet[];

	ActiveEnnemy dyingEnnem = ennemArray[enmIdx];

	int sprSize;
	X_TO_SPRITE_SIZE(sprSize,ennemySheet[dyingEnnem.ennemyID]);

	//Pickup spawning !! TODO !! migrate to a more appropriate place
	//int puSpwnChance = ennemySheet[dyingEnnem.ennemyID].startHP;
	//puSpwnChance = puSpwnChance * puSpwnChance;
	//if (rand() % 256 < puSpwnChance)
	//	addRandPickup(dyingEnnem.xpos, dyingEnnem.ypos);

	oamClone[dyingEnnem.sindex].spriteLoc = 0;
	oamClone[dyingEnnem.sindex].shape = ATR_SQUARE;
	oamClone[dyingEnnem.sindex].spriteSize = ATR_SIZE8;
	giveSindex(dyingEnnem.sindex);
	freeSprite(dyingEnnem.spriteIndex, sprSize);
	extractCompArray(ennemArray,enmCount,enmIdx);
}

void addEnnemy(ActiveEnnemy* enm)
{
	extern const Ennemy ennemySheet[];
	extern ActiveEnnemy* ennemArray;
	extern u8 enmCount;

	Ennemy requested = ennemySheet[enm->ennemyID];
	int sprSize; X_TO_SPRITE_SIZE(sprSize,requested);

	u16 newInx = allocSprite(requested.sprite, sprSize);
	int sindex; getSindex(sindex);

	enm->sindex = sindex;
	enm->spriteIndex = newInx;

	oamClone[sindex].spriteLoc = newInx * 2;
	oamClone[sindex].spriteSize = requested.spriteSize;
	oamClone[sindex].shape = requested.shape;

	oamClone[sindex].xcoord = enm->xpos >> 8;
	oamClone[sindex].ycoord = enm->ypos >> 8;

	insertCompArray(ennemArray,enmCount,*enm);
}

u8 actvEnnemyCount()
{
	extern u8 enmCount; return enmCount;
}

int inHitBox(ActiveEnnemy* enm, int xpos, int ypos)
// Returns 1 if (xpos,ypos) inside enm's bounding box 0 otherwise.
// Note: makes sure that the bounding boxes follow the ship's sprite shape.
{
	int upLx = enm->xpos;
	int upLy = enm->ypos;
	int downRx, downRy;
	{
		int delta;
		const enum atr_ss enmss = ennemySheet[enm->ennemyID].spriteSize;
		const enum atr_sh enmsh = ennemySheet[enm->ennemyID].shape;

		switch (enmss) {
		default:
		case ATR_SIZE8:
			delta = 8 << 8;
			break;
		case ATR_SIZE16:
			delta = 16 << 8;
			break;
		case ATR_SIZE32:
			delta = 32 << 8;
			break;
		case ATR_SIZE64:
			delta = 64 << 8;
			break;
		}
		switch (enmsh) {
		default:
		case ATR_SQUARE:
			downRx = upLx + delta;
			downRy = upLy + delta;
			break;
		case ATR_TALL:
			downRx = upLx + delta;
			downRy = upLy + delta * 2;
			break;
		case ATR_WIDE:
			downRx = upLx + delta * 2;
			downRy = upLy + delta;
			break;
		}
	}
	if (xpos < downRx && xpos > upLx && ypos < downRy && ypos > upLy) {
		extern const Ennemy ennemySheet[];
		return testInsideBox(
			(xpos - upLx) >> 8,
			(ypos - upLy) >> 8,
			ennemySheet + enm->ennemyID
		);
	}
	else
		return 0;
}

static int testInsideBox(int x, int y, const Ennemy* subject)
// Returns 1 if (x,y) is inside the "precise" bounding boxes of subject
// returns 0 otherwise
{
	if (subject->hitbox == NULL)
		return 1; //no specific hitbox set, default one used
		//The default IS the box takes the full sprite box.

	int remBoxes = subject->hitbox->boxCount;
	const struct Box* curBox = subject->hitbox->boxList;
	while (remBoxes != 0) {
		int upLx = curBox->x;
		int upLy = curBox->y;
		int downRx = upLx + curBox->l;
		int downRy = upLy + curBox->h;
		if (x > upLx && y > upLy && x < downRx && y < downRy)
			return 1; //if inside current box, then is indeed hitting it

		curBox++;
		remBoxes--;
	}
	return 0; //Didn't fit inside any box, it isn't touching the ship
}

void AImodify(ActiveEnnemy* enm, PlayerShip* pship)
{
	extern const Ennemy ennemySheet[];
	ennemySheet[enm->ennemyID].AIfunction(enm, pship);
}


void ennemyDisplay(int ennemyLoc)
//Doesn't do validity check, needs to be done by caller
{
	extern ActiveEnnemy* ennemArray;
	ActiveEnnemy curEnnemy = ennemArray[ennemyLoc];

	int expos = curEnnemy.xpos >> 8;
	int eypos = curEnnemy.ypos >> 8;

	if (expos < 256 && expos >= -16 && eypos < 256 && eypos >= -64) {
		//Only display stuff that is inside boundaries
		oamClone[curEnnemy.sindex].xcoord = expos;
		oamClone[curEnnemy.sindex].ycoord = eypos;
	}
}

static void ennemyShoot(ActiveEnnemy* enm, int hspeed, int vspeed, int bulletID)
{
	extern ActiveEnnemy* ennemArray;

	ActiveBullet newBullet = {
		enm->xpos,
		enm->ypos,
		hspeed,
		vspeed,
		bulletID,
		1, //Might need adjustement later
		0
	}; addBullet(&newBullet);
}


/* -------------- *
 *  AI functions  *
 * -------------- */


void stdAI(ActiveEnnemy* enm, PlayerShip* ps)
//goes forward while shooting forward at regular intervals
{
	enm->xpos += enm->hspeed;
	enm->ypos += enm->vspeed;

	if (enm->AIcycle == 0) {
		extern const Ennemy ennemySheet[];
		ennemyShoot(enm, -200, 0, ennemySheet[enm->ennemyID].projectile);
		enm->AIcycle = 360;
	}
	else
		enm->AIcycle -= 1;
}

void stdWaveAI(ActiveEnnemy* enm, PlayerShip* ps)
//Does the waveKamikazeAI but also shoots forward
{
	waveKamikazeAI(enm, ps);
	if (enm->vspeed == 0) {
		extern const Ennemy ennemySheet[];
		ennemyShoot(enm, -200, 0, ennemySheet[enm->ennemyID].projectile);
	}
}

void spinShootAI(ActiveEnnemy* enm, PlayerShip* ps)
//shoots in all directions in a spiral pattern and goes forward
{
	enm->xpos += enm->hspeed;
	enm->ypos += enm->vspeed;

	enm->AIcycle += 1;
	#define SPIN_SHOOTCOOL 16
	if (enm->AIcycle % SPIN_SHOOTCOOL == 0) {
		signed short vspeed, hspeed;
		switch (enm->AIcycle) {//emulates shooting in a circle pattern
		default:
		case SPIN_SHOOTCOOL:
			hspeed = 80;  vspeed = -180; break;
		case SPIN_SHOOTCOOL*2:
			hspeed = 140; vspeed = -140; break;
		case SPIN_SHOOTCOOL*3:
			hspeed = 180; vspeed = -80; break;
		case SPIN_SHOOTCOOL*4:
			hspeed = 200; vspeed = 0; break;
		case SPIN_SHOOTCOOL*5:
			hspeed = 180; vspeed = 80; break;
		case SPIN_SHOOTCOOL*6:
			hspeed = 140; vspeed = 140; break;
		case SPIN_SHOOTCOOL*7:
			hspeed = 80;  vspeed = 180; break;
		case SPIN_SHOOTCOOL*8:
			hspeed = 0;   vspeed = 200; break;
		case SPIN_SHOOTCOOL*9:
			hspeed = -80; vspeed = 180; break;
		case SPIN_SHOOTCOOL*10:
			hspeed = -140;vspeed = 140; break;
		case SPIN_SHOOTCOOL*11:
			hspeed = -180;vspeed = 80; break;
		case SPIN_SHOOTCOOL*12:
			hspeed = -200;vspeed = 0; break;
		case SPIN_SHOOTCOOL*13:
			hspeed = -180;vspeed = -80; break;
		case SPIN_SHOOTCOOL*14:
			hspeed = -140;vspeed = -140; break;
		case SPIN_SHOOTCOOL*15:
			hspeed = -80;vspeed = -180; break;
		case SPIN_SHOOTCOOL*16:
			hspeed = 0;  vspeed = -200; break;
		}
		ennemyShoot(enm, hspeed, vspeed, 5);
		if (enm->AIcycle == SPIN_SHOOTCOOL*16)
			enm->AIcycle = 0;
	}
}

void turretAI(ActiveEnnemy* enm, PlayerShip* ps)
//Tries to find a random spot in the trajectory that is on screen and
// moves in that direction until it reaches it. Otherwise will
// find a random spot on the screen and moves toward it.
// Shoots forward at medium intervals untill it reaches the spot and then
// Start shooting toward the player.
{
	#define RD_SPOT_TRY_MAXL 10

	int onScreen = 0; //if the spot is indeed on screen.

	int xpos = enm->xpos;
	int ypos = enm->ypos;
	int vspeed = enm->vspeed;
	int hspeed = enm->hspeed;
	int tries = RD_SPOT_TRY_MAXL;
	int randMov;

	do {
	//How this work: generates a random value which is the time the ship
	// will spend moving. It then verify that the spot where the ship
	// stops is indeed on-screen. If not, retires up to RD_SPOT_TRY_MAXL
	// times, if it failed to find a proper spot, it will
	// fallback on guarenteed spot on screen outside of this loop
		randMov = 100 + rand() % 3600;
		int tryXpos = (xpos + hspeed * randMov) >> 8;
		int tryYpos = (ypos + vspeed * randMov) >> 8;

		if (tryXpos > 90 && tryXpos < 240 &&
		  tryYpos > 24 && tryYpos < 160) {
			onScreen = 1;
		}
	} while(!onScreen && --tries != 0);

	if (tries == 0)
	//Failed to setup a proper random spot
	//needs to follow a default behavior
	{

	}
}

void zippyAI(ActiveEnnemy* enm, PlayerShip* ps)
// Quickly moves on a vertical line, shooting streight in burst
{

}

void swirrlyAI(ActiveEnnemy* enm, PlayerShip* ps)
// Occupies right side of the screen and moves in circles
// shots at current location of the player
{

}

void dodgeAI(ActiveEnnemy* enm, PlayerShip* ps)
//Avoids horizontal position of the player, shoots only when
// crosses sight with player. Initial vspeed is the speed modifier
{
	int shootingCycle = (enm->AIcycle & 0xFFFF0000) >> 16;
	int pollingCycle = enm->AIcycle & 0x0000FFFF;

	if (enm->AIextraData == 0)
		enm->AIextraData = abs(enm->vspeed);

	// speed modifiers: avoids going out of screen and avoids player
	int eypos = enm->ypos;
	int dpos = eypos - ps->ypos;

	if (pollingCycle == 0) {
		// Initial modifier, depending on ennemy vertical position on screen
		//if (eypos < (28 << 8))
		//	enm->vspeed = 6 * enm->AIextraData;
		//else if (eypos > (132 << 8))
		//	enm->vspeed = -6 * enm->AIextraData;
		if (eypos < (56 << 8)) // 28: y at which enm moves down by 8
			enm->vspeed = (16 - (eypos>>11)) * enm->AIextraData;
		else if (eypos > (120 << 8))
			enm->vspeed =  (120<<8) - eypos ;// * enm->AIextraData;
		else
			enm->vspeed = 0;
	// Player induced modifier
		if (dpos > (-6 << 8) && dpos < 0) //enm higher and close to player
			enm->vspeed += 5 * enm->AIextraData;
		else if (dpos < (6 << 8)) //enm under player and close to it
			enm->vspeed += -5 * enm->AIextraData;
		else if (dpos > ( -32 << 8) && dpos < 0) //enm higher and far from ps
			enm->vspeed += -3 * enm->AIextraData;
		else if (dpos < (32 << 8) && dpos > 0) //enm under player and far
			enm->vspeed += 3 * enm->AIextraData;

		enm->AIcycle &= ~0xFFFF; enm->AIcycle |= 40 & 0xFFFF;
	} else {
		enm->AIcycle &= ~0xFFFF; enm->AIcycle |= (pollingCycle - 1) & 0xFFFF;
	}
	if (abs(dpos) < (2 << 8) && shootingCycle == 0) {//shoots if crosses
	//player sight
		enm->AIcycle &= ~0xFFFF0000; enm->AIcycle |= (80 << 16) & 0xFFFF0000;
		shootingCycle = 80;
	}
	if (shootingCycle == 80 ||
	  shootingCycle == 60 ||
	  shootingCycle == 40) { //shooting cycle
		extern const Ennemy ennemySheet[];
		ennemyShoot(enm, -100,1, ennemySheet[enm->ennemyID].projectile);
	}
	if (shootingCycle != 0) {
		enm->AIcycle &= ~0xFFFF0000;
		enm->AIcycle |= ((shootingCycle - 1) << 16) & 0xFFFF0000;
	}
	enm->ypos += enm->vspeed;
}

void sneakySpammerAI(ActiveEnnemy* enm, PlayerShip* ps)
//Avoids horizontal position of the player, shoots only when
// crosses sight with player. Initial vspeed is the speed modifier
{
	int AIcycle = enm->AIcycle;

	if (enm->AIextraData == 0)
		enm->AIextraData = abs(enm->vspeed);

	// speed modifiers: avoids going out of screen and avoids player
	int eypos = enm->ypos;
	int dpos = eypos - ps->ypos;

	// Initial modifier, depending on ennemy vertical position on screen
	if (eypos < (28 << 8))
		enm->vspeed = 6 * enm->AIextraData;
	else if (eypos > (132 << 8))
		enm->vspeed = -6 * enm->AIextraData;
	else if (eypos < (48 << 8))
		enm->vspeed = 2 * enm->AIextraData;
	else if (eypos > (112 << 8))
		enm->vspeed = -2 * enm->AIextraData;

	// Player induced modifier
	if ( dpos > (-8 << 8) && dpos < 0) //enm higher and close to player ship
		enm->vspeed += 5 * enm->AIextraData;
	else if (dpos < (8 << 8) && dpos > 0) //enm under player and close to it
		enm->vspeed += -5 * enm->AIextraData;
	else if (dpos > ( -24 << 8) && dpos < 0) //enm higher and far from player
		enm->vspeed += 3 * enm->AIextraData;
	else if (dpos < (24 << 8) && dpos > 0) //enm under player and far from him
		enm->vspeed += -3 * enm->AIextraData;

	if (abs(dpos) < (8<<8) && AIcycle == 0) {//shoots if crosses player sight
		enm->AIcycle = 40;
		AIcycle = 40;
	}

	if (AIcycle == 40 ||
	  AIcycle == 30 ||
	  AIcycle == 20 ||
	  AIcycle == 10) {//shooting cycle
		extern const Ennemy ennemySheet[];
		ennemyShoot(enm, -100,1,ennemySheet[enm->ennemyID].projectile);
	}
	if (AIcycle != 0) enm->AIcycle = AIcycle - 1;
	enm->ypos += enm->vspeed;
}

void lineKamikazeAI(ActiveEnnemy* enm, PlayerShip* ps)
//goes forward untill going out of screen, does not shoot
{
	enm->xpos += enm->hspeed;
	enm->ypos += enm->vspeed;
}

void waveKamikazeAI(ActiveEnnemy* enm, PlayerShip* ps)
//Goes forward doing cosin looking shapes, does not shoot
//uses initial vertical speed as the one on the origine line
{
	int vspeed = enm->vspeed;
	if (enm->AIextraData == 0)
		enm->AIextraData = abs(vspeed);

	enm->xpos += enm->hspeed;
	enm->ypos += vspeed;

	//When close to the horizontal line, needs to go faster, slows down
	//untill 0 then speed in the other direction when reaching maximum
	//we can do a periodic function where each periodic element is the
	//Same polynomial function of degree 2 (aka vspeed the derivative is)
	//linear, and the added value here is constant
	if (vspeed >= enm->AIextraData)
		enm->AIcycle = -2;
	else if ( vspeed <= -(enm->AIextraData))
		enm->AIcycle = 2;

	enm->vspeed += enm->AIcycle;
}

void lockerAI(ActiveEnnemy* enm, PlayerShip* ps)
//always stays in the same horizontal line as the player, shoots forward
//Does not move horizontally
{
	//Stores the initial vspeed as modifier for speed of the ship
	if (enm->AIextraData == 0)
		enm->AIextraData = abs(enm->vspeed);

	//vv movement needed to track player
	enm->vspeed = ((ps->ypos - enm->ypos) * enm->AIextraData) >> 8;

	if ((enm->AIcycle += 2) >= 480) {
		extern const Ennemy ennemySheet[];

		enm->AIcycle = 0;
		ennemyShoot(enm, -240, 0, ennemySheet[enm->ennemyID].projectile);
	}
	enm->ypos += enm->vspeed;
}

void smartAI(ActiveEnnemy* enm, PlayerShip* ps)
// Shoots where the the player will be when thebullet reaches their location
// Faster bullets than standard, though uses same movement AI
{

}

void randomAI(ActiveEnnemy*  enm, PlayerShip* ps)
// Chooses random directions to move toward every two seconds, shoots at random
//Intervals toward a random location close to the player
{

}

void spinnerShooterAI(ActiveEnnemy* enm, PlayerShip* ps)
{
	extern u8 enmCount;

	if (enm->xpos >= 120) {
		enm->xpos += enm->hspeed;
		enm->ypos += enm->vspeed;
	}
	if (enm->AIcycle == 0 && enmCount == 1) {
		ActiveEnnemy newEnnemy = {
			21, 0, 0,
			enm->xpos + (16 << 8),
			enm->ypos + (16 << 8),
			0,
			-10,
			ennemySheet[21].startHP,
			0,0,0,0
		}; addEnnemy(&newEnnemy);
		enm->AIcycle = 1200;
	} else {
		enm->AIcycle -= 1;
	}
}
