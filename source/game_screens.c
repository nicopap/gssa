#include "game_screens.h"

#include "exit_debug.h"

extern void u8drawFrame(u16* loc, u8 tile, u32 width, u32 height);
extern void u8drawText(u16* loc, char* text);
extern void u8drawPic(u8* loc, u8 tile, u32 L, u32 H, u32 width, u32 mapwidth);
extern void u8drawMos(u8* loc, u8 tile, u32 L, u32 H, u32 mapwidth);

static void drawPause(u32*, u32*);
static void undrawPause(u32, u32);
static void pauseLoop(u32);
static void __u8drawFrame(int x, int y, int width, int heigh);
static void __u8drawText(int x, int y, char* text);
static void __u8drawPic(u32 x, u32 y, u8 tile, u32 w, u32 l);
static void __u8drawMos(u32 x, u32 y, u8 tile, u32 w, u32 l);

static void moveFrame(enum direction dir, int x, int y);

void screenGameOver()
{
	blueScreen("\n\n\n\n\n\n\n\n           YOU DEAD");
}


void screenLevelComplete()
{
	blueScreen("\n\n\n\n\n\n\n\n           YOU WONE");
}

void screenPause()
//Writes the pause screen on BG0.
{
	u32 tm0Stat, tm1Stat;
	drawPause(&tm0Stat, &tm1Stat);
	WAIT_SEMI_FRAME;

	pauseLoop(REG_KEYVIEW);
	undrawPause(tm0Stat, tm1Stat);
}

static void drawPause(u32 *tm0Status, u32 *tm1Status)
//Freezes timers, enables BG3 rendering, draws pause menu
//Additionaly could draw some player stats
{
	*tm0Status = REG_TM0CNT;
	*tm1Status = REG_TM1CNT;
	REG_TM0CNT &= ~TIMER_ENABLE;
	REG_TM1CNT &= ~TIMER_ENABLE;

	__u8drawFrame(9,6,10,1);
	__u8drawText(10,7,"Pause menu");
	REG_DISPCNT |= BG3_ENABLE;
}

static void undrawPause(u32 tm0Status, u32 tm1Status)
//Unfreezes timers, disables BG3 rendering, undraws pause menu
{
	REG_DISPCNT &= ~BG3_ENABLE;
	REG_TM1CNT = tm1Status;
	REG_TM0CNT = tm0Status;
}

static void pauseLoop(u32 oldKeyconf)
//Handles player input within the pause menu
//Can: resume game, reset game, go to menu screen
{
	int curx = 9;
	int cury = 6;
	for (;;) {
		u32 curKeyconf = REG_KEYVIEW;
		if (curKeyconf != oldKeyconf) {//User input changed
			if ((~curKeyconf & KEY_START) == KEY_START)
				return;
			if ((~curKeyconf & KEY_DOWN) == KEY_DOWN) {
				moveFrame(DIR_down, curx, cury);
				cury += 1;
			} else if ((~curKeyconf & KEY_UP) == KEY_UP) {
				moveFrame(DIR_up, curx, cury);
				cury -= 1;
			} else if ((~curKeyconf & KEY_LEFT) == KEY_LEFT) {
				moveFrame(DIR_left, curx, cury);
				curx -= 1;
			} else if ((~curKeyconf & KEY_RIGHT) == KEY_RIGHT) {
				moveFrame(DIR_right, curx, cury);
				curx += 1;
			}
			oldKeyconf = curKeyconf;
		}
		WAIT_SEMI_FRAME;
	}
}

static void moveFrame(enum direction dir, int oldx, int oldy)
{
	__u8drawMos(oldx, oldy, 0, 12, 3);
	switch (dir) {
	case DIR_up:
		__u8drawFrame(oldx, oldy-1, 10, 1);
		__u8drawText(oldx+1, oldy, "Pause menu");
		break;
	case DIR_down:
		__u8drawFrame(oldx, oldy+1, 10, 1);
		__u8drawText(oldx+1, oldy+2, "Pause menu");
		break;
	case DIR_left:
		__u8drawFrame(oldx-1, oldy, 10, 1);
		__u8drawText(oldx, oldy+1, "Pause menu");
		break;
	case DIR_right:
		__u8drawFrame(oldx+1, oldy, 10, 1);
		__u8drawText(oldx+2, oldy+1, "Pause menu");
		break;
	}
}

static void __u8drawFrame(int x, int y, int w, int h)
//Wraper to use the assembly function drawFrame
{
	#define UPLFRAME 96
	// TILESETU8(x) (((u8*)0x6000000) + ((x) << 11))
	u16* loc = TILESETU8(UI_SBB) + (x + 32*y);
	u8drawFrame(loc, (u8) UPLFRAME, w, h);
}

static void __u8drawText(int x, int y, char* text)
{
	u16* loc = TILESETU8(UI_SBB) + (x + 32*y);
	u8drawText(loc, text);
}

#define MAP_SIZE 32
static void __u8drawPic(u32 x, u32 y, u8 tile, u32 w, u32 l)
{
	#define TILE_SIZE 32
	u8* loc = TILESETU8(UI_SBB) + (x + MAP_SIZE * y);
	u8drawPic(loc, tile, w, l, TILE_SIZE, MAP_SIZE);
}

static void __u8drawMos(u32 x, u32 y, u8 tile, u32 w, u32 l)
{
	u8* loc = TILESETU8(UI_SBB) + (x + MAP_SIZE * y);
	u8drawMos(loc, tile, w, l, MAP_SIZE);
}
