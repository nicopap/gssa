@ Draws the text of length len into memory at loc.
@ Please don't do very long text or 0 length text. You are a programmer, you
@ know what is helthy for you.
@ u8drawText(u16* loc, u8* text);
@ r0 = location
@ r1 = textPointer

  .ARM
  .ALIGN
  .GLOBAL u8drawText

u8drawText:
  STMFD sp!,{r4, lr} @ Save register r4 to r7 on stack
  ANDS r3, r0, #1 @if r0 % 2 != 0
    LDRNEH r4, [r0, #-1]
    ANDNE r4, r4, #0xFF

  writeLetter:
    LDRB r2, [r1], #1
    CMP r2, #0
    BEQ endWrite
    SUB r2, r2, #32
    ANDS r3, r0, #1 @if r0 % 2 == 0
      MOVEQ r4, r2
    @else
      MOVNE r3, r2, LSL #8
      ORRNE r4, r4, r3
      STRNEH r4, [r0, #-1]
    ADD r0, r0, #1
    B writeLetter
  endWrite:
  ANDS r3, r0, #1 @if r0 % 2 != 0
    LDRNEH r3, [r0, #-1]
    ANDNE r3, r3, #0xFF00
    ORRNE r4, r4, r3
    STRNEH r4, [r0, #-1]
  LDMFD sp!,{r4, pc} @ Restore registers r4 to r7 and MOV pc, lr
