
#include "bgutil.h"

void cyclePalette(u32 startColor, u32 endColor)
{
	u16 temp = BACKGROUND_PALETTE[startColor];

	do {
		BACKGROUND_PALETTE[startColor] = BACKGROUND_PALETTE[startColor+1];
		startColor++;
	} while (startColor < endColor);
	BACKGROUND_PALETTE[endColor] = temp;
}


void drawMos(u32 tile, u32 xpos, u32 ypos, u32 w, u32 h, u32 mapLoc)
{
	for (int i = 0; i < h; i++)
		for (int j = 0; j < w; j++)
			BG_TILEMAP(mapLoc)[(xpos+j) + (ypos+i)*32].xcoord = tile;
}

void drawText(char* text, u32 xpos, u32 ypos, u32 mapLoc)
{
	int i = 0;
	char nxtChar;
	while( (nxtChar = text[i++])!= '\0')
		BG_TILEMAP(mapLoc)[(xpos++) + ypos*32].xcoord = CHAR_TILE(nxtChar);
}


void drawPic(u32 spriteLoc, u32 w, u32 h, u32 xpos, u32 ypos, u32 mapLoc)
/* Draws the sprite of size w x h at position tiles xpos ypos on screen
 * starting at spriteLoc */
{
	for (int y = ypos; y < ypos+h; y++ , spriteLoc += (32 - w))
	{
		for (int x = xpos; x < xpos+w; x++)
			BG_TILEMAP(mapLoc)[x+ (y<<5)].xcoord = spriteLoc++;
	}
}
