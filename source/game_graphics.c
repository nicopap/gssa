#include "game_graphics.h"

#include "sprite_handler.h"

//Graphic files:

//Star background and background palette
#include "gamesetbg_til_bin.h"
#include "gamesetbg_pal_bin.h"

//UI background
#include "gamesetui_til_bin.h"

//Foreground: ennemy ships, player ship, powerups, bullets
#include "gamesetfg_pal_bin.h"

//Dynamic palette extra colors
#include "pbull_lv0_pal_bin.h" //bullets
#include "pbull_lv1_pal_bin.h"
#include "pbull_lv2_pal_bin.h"
#include "pbull_lv3_pal_bin.h"

#define STAR_SBB 20
#define ENNEMY_BULLET_POS 32

extern void u8drawPic(u8* loc, u8 tile, u32 L, u32 H, u32 width,u32 mapwidth);

void genbg();
static void darkenPalette(int startingPoint, int endingPoint);
static void funkyenPalette(int start, int end);
static void __u8drawPic(u32 x, u32 y, u8 tile, u32 size);

void loadGraphics()
{
	#define BG_PALETTE BACKGROUND_PALETTE
	#define FG_PALETTE SPRITE_PALETTE

	//Setup master graphic controller
	REG_DISPCNT = VID_MODE_2 | BG2_ENABLE | OBJ_ENABLE | OBJ_MAP_1D;
	initSprites();

	//Setup bg2
	volatile bgcnt* bg2cnt = (volatile bgcnt*) REG_BG2CNT;
	bg2cnt->overflow = 1; //Uses repeated background
	bg2cnt->screenBaseBlock = STAR_SBB; //location of tile map
	bg2cnt->priority = 2; //lowest priority
	bg2cnt->paletteType = 1;
	bg2cnt->size = 1; //Makes the background a tile map of 32x32 tiles.

	//first row of bg palt (first 16 colors) are reserved for player ship colors
	//(and maybe players shooty weapon, if there is enough place)
	DMAcopy((void*) BG_PALETTE, (void*) gamesetbg_pal_bin, 96,DMA16NOW);
	DMAcopy((void*) TILE_SHEET, (void*) gamesetbg_til_bin, 7168, DMA16NOW);


	//Setup bg3
	volatile bgcnt* bg3cnt = (volatile bgcnt*) REG_BG3CNT;
	bg3cnt->overflow = 1;
	bg3cnt->screenBaseBlock = UI_SBB; //tile map location
	bg3cnt->priority = 0;
	bg3cnt->paletteType = 1;
	bg3cnt->size = 1;
	bg3cnt->charBase = 1; //char base for the tileset.
	DMAcopy((void*) TILE_SHEET_BK1, (void*) gamesetui_til_bin, 3328, DMA16NOW);

	genbg();

	//Setup sprite memory
	DMAcopy((void*) FG_PALETTE, (void*) gamesetfg_pal_bin, 256, DMA16NOW);

	//darkenPalette(16,46);
}


void cycleGraphics(int scrollsp)
{
	static u32 currentOffset;
	static u32 currentFrame;
	if (++currentFrame  == 64) {
		currentFrame = 0;
		cyclePalette(16, 22); //Star colors
	}
	u32 startColor;
	u16 temp;

	if ((currentFrame & 0x7) == 0) {
		startColor = ENNEMY_BULLET_POS; //Ennemy bullet color cycling
		u16 temp = FG_PALETTE[startColor];

		FG_PALETTE[startColor] = FG_PALETTE[startColor+1];
		startColor++;
		FG_PALETTE[startColor] = FG_PALETTE[startColor+1];
		startColor++;
		FG_PALETTE[startColor] = FG_PALETTE[startColor+1];
		startColor++;
		FG_PALETTE[startColor] = FG_PALETTE[startColor+1];
		startColor++;
		FG_PALETTE[startColor] = FG_PALETTE[startColor+1];
		startColor++;
		FG_PALETTE[startColor] = FG_PALETTE[startColor+1];
		startColor++;
		FG_PALETTE[startColor] = FG_PALETTE[startColor+1];
		startColor++;

		FG_PALETTE[startColor] = temp;
	}
	if ((currentFrame & 0xF) == 0) {//player bullet color cycling
		startColor = PLAYER_BULLET_POS;
		temp = FG_PALETTE[startColor];

		FG_PALETTE[startColor] = FG_PALETTE[startColor+1];
		startColor++;
		FG_PALETTE[startColor] = FG_PALETTE[startColor+1];
		startColor++;
		FG_PALETTE[startColor] = FG_PALETTE[startColor+1];
		startColor++;
		FG_PALETTE[startColor] = FG_PALETTE[startColor+1];
		startColor++;
		FG_PALETTE[startColor] = FG_PALETTE[startColor+1];
		startColor++;

		FG_PALETTE[startColor] = temp;
	}

	currentOffset += scrollsp;
	*(int*) BG2X_L = currentOffset;
}


void genbg()
// background generation algorithm:
// first drafts 1x1 tile objects, as it wont be cut by other itterations of the
// algorithm. It then places 2 big sprites on a 4x4 grid. It then places
// randomly the medium sprites on the remaining grid blocks.
// !! NOTE !! The background needs to be reregenerated at the beginning of each
//   level. Also, regen needs to not be accomplished uppon player death
{
	#define ST_RRTY 8
	#define PL_RRTY 32

	u32 frameSeed = rand();
	u32 selectSeed = rand();

	for (u32 curTile = 0; curTile < 1024; curTile++) {
	// creates first draft with only one tile sprites
	// randomly generated star bg
		if (curTile % 2 == 0) {
			BG_TILESMALL(STAR_SBB)[curTile/2] = ((selectSeed % ST_RRTY) != 0)?
			                                    0 :
			                                    (frameSeed % 16) & 0x00ff;
		} else {
			BG_TILESMALL(STAR_SBB)[curTile/2] |= ((selectSeed % ST_RRTY) != 0)?
			                                      0 :
			                                   ((frameSeed % 16) << 8) & 0xff00;
		}
		if ((frameSeed >>= 8) == 0)
			frameSeed = rand();
		if ((selectSeed >>= 4) == 0)
			selectSeed = rand();
	}
	selectSeed = rand();
	u32 bigTiles = rand() & 0x3F3F3F3F;//generates 4 u8 rand values, used to
	//place the big plannetes on the screen

	#define bt_u8 ((u8*) &bigTiles) //bigTiles represented as a u8 table

	int tilLoc = 0;
	for (u32 curBlock = 0; curBlock < 64; curBlock++, tilLoc += 2) {
		if ( bt_u8[0] == curBlock || bt_u8[1] == curBlock ||
			 bt_u8[2] == curBlock || bt_u8[3] == curBlock ) {
		//Check if in the current block is one of the two that has been choosen
			u32 curPlan = 96 + 4 * (selectSeed % 4);
			u32 tlc = tilLoc;

			BG_TILESMALL(STAR_SBB)[tlc++] = curPlan | ((curPlan+1) << 8);
			curPlan += 2;
			BG_TILESMALL(STAR_SBB)[tlc] = curPlan | ((curPlan+1) << 8);
			curPlan += 30;
			tlc += 15;

			BG_TILESMALL(STAR_SBB)[tlc++] = curPlan | ((curPlan+1) << 8);
			curPlan += 2;
			BG_TILESMALL(STAR_SBB)[tlc] = curPlan | ((curPlan+1) << 8);
			curPlan += 30;
			tlc += 15;

			BG_TILESMALL(STAR_SBB)[tlc++] = curPlan | ((curPlan+1) << 8);
			curPlan += 2;
			BG_TILESMALL(STAR_SBB)[tlc] = curPlan | ((curPlan+1) << 8);
			curPlan += 30;
			tlc += 15;

			BG_TILESMALL(STAR_SBB)[tlc++] = curPlan | ((curPlan+1) << 8);
			curPlan += 2;
			BG_TILESMALL(STAR_SBB)[tlc] = curPlan | ((curPlan+1) << 8);
		} else {
			if ((selectSeed & 0x8) == 0) {//Puts a props only if he is allowed
				//Chooses a random spot in the block to past prop
				u32 tlc= tilLoc +((selectSeed&0x2)>>1) + ((selectSeed&0x1)*64);

				u32 curProp = 32 + 2 * ( ((selectSeed & 0xF0)>>4) % 8);

				BG_TILESMALL(STAR_SBB)[tlc] = curProp | ((curProp+1) << 8);
				tlc += 16;
				curProp += 32;
				BG_TILESMALL(STAR_SBB)[tlc] = curProp | ((curProp+1) << 8);
			}
		}
		if ((curBlock % 8) == 7)
			tilLoc += 126;

		if ((selectSeed >>= 8) == 0)
			selectSeed = rand();
	}
	//__u8drawPic(4,4,1,4);
}

static void darkenPalette(int start, int end)
{
	for (int i = start; i <= end; i++) {
		u16 curCol = BG_PALETTE[i];
		int curRed = ((curCol & (31<<1)) >> 1) & 31;
		int curGreen = ((curCol & (31<<6)) >> 6) & 31;
		int curBlue = ((curCol & (31<<11)) >> 11) & 31;
		#define DARK_AMMOUNT 6
		int avgCol = (curRed + curGreen + curBlue) / 6;

		BG_PALETTE[i] =
		         RGB((avgCol+curRed/2),(avgCol+curGreen/2),(avgCol+curBlue/2));
	}
}

static void funkyenPalette(int start, int end)
{
	for (int i = start; i <= end; i++) {
		u16 curCol = BG_PALETTE[i];
		int curRed = ((curCol & (31<<1)) >> 1) & 31;
		int curGreen = ((curCol & (31<<6)) >> 6) & 31;
		int curBlue = ((curCol & (31<<11)) >> 11) & 31;
		#define DARK_AMMOUNT 6
		int avgCol = (curRed + curGreen + curBlue) / 6;

		BG_PALETTE[i] = RGB(avgCol + curRed/2,
		                    avgCol + curGreen/2,
		                    avgCol + curBlue/2);
	}
}

static void __u8drawPic(u32 x, u32 y, u8 tile, u32 size)
{
	#define MAP_SIZE 64
	#define TILE_SIZE 32
	u8* loc = TILESETU8(STAR_SBB) + (x + MAP_SIZE * y);
	u8drawPic(loc, tile, size, size, TILE_SIZE, MAP_SIZE);
}
