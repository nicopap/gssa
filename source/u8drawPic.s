@ Draws sprite of size L x H into map mem at location loc. The tile mem is
@ supposed to be of total width width. tile is the value of upper left corner
@ of the picture you want to draw.
@ Does not support lower than 2x2 pictures.
@ u8drawPic(u8* loc, u8 tile, u32 L, u32 H, u32 width, u32 mapwidth);
@ r0 = location
@ r1 = tile
@ r2 = length of pic (# of tiles)
@ r3 = height of pic (# of tiles)
@ [sp] = width of tileset
@ [sp, #4] = width of map
@ r4 used as buffer for tile shape
@ r5 used as iterator on width

  .ARM
  .ALIGN
  .GLOBAL u8drawPic

u8drawPic:
  STMFD sp!,{r4-r9, lr} @ Save register r4 to r8 on stack
  LDR r8, [sp, #28] @ r8 = tileWidth
  LDR r9, [sp, #32] @ r9 = mapWidth

  SUB r8, r8, r2 @aligns lengths for future calculations
  SUB r9, r9, r2 @
  ANDS r7, r0, #1 @ if (r0 % 2 == 0)
    BEQ testErightE
    BNE testOrightE
  testErightE:
    ADD r7, r0, r2
    ANDS r7, r7, #1 @ if (r0 % 2 == 0)
      SUBNE r2, r2, #1 @if last column uneven, we setup differently
      ADD r5, r1, r2
      BEQ lineEE
      ADDNE r9, r9, #1
      ADDNE r8, r8, #1
      BNE lineEO
  testOrightE:
    ADD r7, r0, r2
    ANDS r7, r7, #1 @ if (r0 % 2 == 0)
      SUBNE r2, r2, #1 @if last column uneven, we setup differently
      ADD r5, r1, r2
      BEQ lineOE
      ADDNE r9, r9, #1
      ADDNE r8, r8, #1
      BNE lineOO


  lineEE:@means width of picture is even
    tileEE:
      MOV r4, r1
      ADD r1, r1, #1
      ORR r4, r4, r1, LSL #8
      ADD r1, r1, #1
      STRH r4, [r0], #2
      CMP r1, r5 @if we did not reach end of line of picture
      BNE tileEE
    @we set up the next line, check if we reached end of picture
    ADD r1, r1, r8 @go to next row of tile
    ADD r5, r1, r2 @store value of greatest tile in row
    ADD r0, r0, r9 @move pointer to next row on map mem
    SUBS r3, r3, #1 @check if we drew everything
    BNE lineEE @if not, we continue with next row
  B endJob @else we end job


  lineEO:
    tileEO:
      MOV r4, r1
      ADD r1, r1, #1
      ORR r4, r4, r1, LSL #8
      ADD r1, r1, #1
      STRH r4, [r0], #2
      CMP r1, r5 @if we did not reach end of line of picture
      BNE tileEO
    @We finished the line but the last column
    LDRH r4, [r0]       @Loads old value at location of last column
    AND r4, r4, #0xFF00 @and replaces it with half of it with the new
    ORR r4, r4, r1      @value in the first byte.
    STRH r4, [r0], r9   @Plus moves pointer to next row on map mem
    ADD r1, r1, r8 @go to next row of tile
    ADD r5, r1, r2 @store value of greatest tile in row
    SUBS r3, r3, #1 @check if we reached last line
    BNE lineEO @if not, we draw the next one
  B endJob


  lineOE:
    LDRH r4, [r0,#-1]      @Loads first column
    AND r4, r4, #0xFF      @with a mask and everything
    ORR r4, r4, r1, LSL #8 @
    STRH r4, [r0,#-1]      @
    ADD r0, r0, #1
    ADD r1, r1, #1
    tileOE:
      MOV r4, r1
      ADD r1, r1, #1
      ORR r4, r4, r1, LSL #8
      ADD r1, r1, #1
      STRH r4, [r0], #2
      CMP r1, r5 @if we did not reach end of line of picture
      BNE tileOE
    @We finished the line
    ADD r1, r1, r8 @go to next row of tile
    ADD r5, r1, r2 @store value of greatest tile in row
    ADD r0, r0, r9 @move pointer to next row on map mem
    SUBS r3, r3, #1 @check if we drew everything
    BNE lineOE @if not, we continue with next row
  B endJob @else we end job


  lineOO:@means width of picture is even
    LDRH r4, [r0,#-1]      @Loads first column
    AND r4, r4, #0xFF      @with a mask and everything
    ORR r4, r4, r1, LSL #8 @
    STRH r4, [r0,#-1]      @
    ADD r0, r0, #1
    ADD r1, r1, #1
    tileOO:
      MOV r4, r1
      ADD r1, r1, #1
      ORR r4, r4, r1, LSL #8
      ADD r1, r1, #1
      STRH r4, [r0], #2
      CMP r1, r5 @if we did not reach end of line of picture
      BNE tileOO
    @We finished the line but the last column
    LDRH r4, [r0]       @Loads old value at location of last column
    AND r4, r4, #0xFF00 @and replaces it with half of it with the new
    ORR r4, r4, r1      @value in the first byte.
    STRH r4, [r0], r9   @Plus moves pointer to next row on map mem
    ADD r1, r1, r8 @go to next row of tile
    ADD r5, r1, r2 @store value of greatest tile in row
    SUBS r3, r3, #1 @check if we reached last line
    BNE lineOO @if not, we draw the next one
  B endJob


  endJob:
    LDMFD sp!,{r4-r9, pc} @ Restore registers r4 to r8 and MOV pc, lr
