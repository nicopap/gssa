#include "gba_util.h"

#define OAM 0x07000000

void DMAcopy(void* dst, const void* src, u16 size, u16 type)
/* sends request to DMA to copy from src into dst
 * size bytes. type is the kind of DMA copy to execute */
{
	REG_DMA3SAD = (u32) src;
	REG_DMA3DAD = (u32) dst;
	REG_DMA3CNT_L = size;
	REG_DMA3CNT_H = type;
}


void vcountHandler()
{
	DMAcopy((void*) OAM, (void*) oamClone, 512, DMA16NOW);
}


void interHandler()
/* handles interruptions */
{
	REG_IME = 0x00;
	u16 oldInterrupts = REG_IF;
	if (TST_FG(oldInterrupts, INT_VCOUNT))
		vcountHandler();

	REG_IF = oldInterrupts;
	REG_IME = 1;
}


void setupOAMinterrupt()
{
	REG_IME = 0x00; //Set interrupts for vblank and timers
	  REG_INTRPT = (u32)interHandler;
	  REG_IE  |= INT_VCOUNT;
	  DISPSTAT_CNT->vCountIntrpt = 1;
	REG_IME = 1;
}


void setupOAM()
{
	for (int i = 0; i < 128; i++) {
		oamClone[i].colorType = ATR_256COL;
		oamClone[i].transpFlag = 1;
		oamClone[i].mosaicFlag = 1;
		oamClone[i].priority = 1; //Middle priority
	}
}
