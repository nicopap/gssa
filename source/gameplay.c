#include <stdbool.h>

#include "gameplay.h"

// a single value of size sizeof(PlayerShip)
PlayerShip PSHIP_MEM;

// An array of size MAX_BULLET * sizeof(ActiveBullet)
ActiveBullet ACTIVBULLET_MEM[MAX_BULLET];

// An array of size MAX_ENNEMY * sizeof(ActiveEnnemy)
ActiveEnnemy ACTIVENNEMY_MEM[MAX_ENNEMY];

// An array of size MAX_PICKUP * sizeof(ActivePickup)
ActivePickup ACTIVPICKUP_MEM[MAX_PICKUP];

#define SCROLL_SPEED 40
#define WAVE_HANG 100
#define WAVES 0, 8, 2, 3, 6, 4, 3
#define WAVE_COUNT 7

#define LEVEL_COMPLETE 1
#define LEVEL_INCOURSE 0



void playGame(GameState* settings)
{
	//setup game here
	PlayerShip* pship = &PSHIP_MEM;
	int levelWaves[] = {WAVES};
	loadGraphics();

	setupOAM();
	initPlayer(pship, settings);
	initBullets((ActiveBullet*) ACTIVBULLET_MEM);
	initEnnemies((ActiveEnnemy*) ACTIVENNEMY_MEM);
	initPickup(settings, (ActivePickup*) ACTIVPICKUP_MEM);
	setupOAMinterrupt();
	//setupWAVEhandler();

//runningGame, gameplay loop
	while(1) {
		playerInputHandler(pship);
		displayPlayer(pship);
		cycleGraphics(SCROLL_SPEED);
		bulletHandler();
		if (waveHandler(levelWaves,WAVE_COUNT) == LEVEL_COMPLETE) {
			screenLevelComplete();
		}
		ennemyHandler(pship);
		pickupHandler();
		collisionHandler(pship);
		WAIT_SEMI_FRAME;
	}
}


/* ------------------- *
 *  Collision handling *
 * ------------------- */

void collisionHandler(PlayerShip* ps)
{
	#define MOD_B2(x,mod) ((x) & ((mod) - 1))
	#define PLAYER_OWNER 0

	ActiveBullet* osBull = ACTIVBULLET_MEM;
	ActiveEnnemy* osEnnem = ACTIVENNEMY_MEM;
	ActivePickup* osPickup = ACTIVPICKUP_MEM;

	//We itterate through bullets and check what they are hitting, because
	//those are uniform, and hold the information of what they can hit
	int pXpos = ps->xpos;
	int pYpos = ps->ypos;

	for (int i = actvBulletCount(); i != 0; i--) {
		ActiveBullet* curBullet = osBull + i - 1;
		int owner = curBullet->owner;
		int Blocx = curBullet->xpos;
		int Blocy = curBullet->ypos;

		if (owner != PLAYER_OWNER) {
		//Case where bullet is from ennemy AND player not invincible
		// if it is thecase, we check if it is touching the player
			if ((Blocx < pXpos + (14 << 8) //This is the player hitbox
			  && Blocx > pXpos + (2 << 8)
			  && Blocy < pYpos + (14 << 8)
			  && Blocy > pYpos + (2 << 8))
			){
				if (ps->iFrames == 0) {
					playerHit(ps);
				}
				killBullet(i-1);
				FREEZ_FRAME;
			}
		} else {
		//Case where bullet is from player we check if the bullet is hitting
		//an ennemy. We need to be aware of the hitbox of the ennemy ship
		//that is being touched.
			for (int j = actvEnnemyCount(); j != 0; j--) {
				ActiveEnnemy* curEnm = osEnnem + (j - 1);
				if (inHitBox(curEnm, Blocx, Blocy)){
					ennemyHit(curEnm, ps); //The bullet broke on the ship, we
					killBullet(i-1); // stop looking for more
					break; //for more (also sovles a game breaking bug)
				}
			}
		}
	}
	for (int i = actvPickupCount(); i != 0; i--) {
		ActivePickup* curPu = osPickup + i - 1;
		if (curPu->type != pt_null
		  && curPu->xpos > pXpos
		  && curPu->xpos < pXpos + (16<<8)
		  && curPu->ypos > pYpos
		  && curPu->ypos < pYpos + (16<<8) ) {
			collectPickup(curPu, ps);
		}
	}
}


/* ------------------------------------- *
 * Ennemies and waves handling functions *
 * ------------------------------------- */

int waveHandler(int waveList[], int waveListSize)
// Spawns next wave of ennemies for this level if the last wave finished.
// The lack of a WAVE_HANG at the beginning of the game might cause issues.
// !! NOTE !! The implementation of the ennemy spawning procedure might cause
//	 The ennemies to spawn OUTSIDE OF THE GAME LOOP!!! We need to repress
//	 timer interrupts when leaving the game loop.
{
	static int curWave;
	static int waveCooldown;
	extern volatile int WAVE_COMPLETION_FLAG;

	bool allDead = (actvEnnemyCount() == 0)? true : false;
	int waveFlag = WAVE_COMPLETION_FLAG;

	testWaveTime();
	if (waveFlag == WCF_OVER && allDead && waveCooldown == WAVE_HANG) {
		waveCooldown = 0;
		if (curWave >= waveListSize) {
			return LEVEL_COMPLETE;
		} else {
			spawnWave(waveList[curWave++]);
			return LEVEL_INCOURSE;
		}
	} else {
		if (waveCooldown != WAVE_HANG)
			waveCooldown += 1;
		return LEVEL_INCOURSE;
	}
}
