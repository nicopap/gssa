
/* note: in menus, A to confirm selection
 *  B to go back to previous screen */


/* Menu composition:
 * Title screen:
 *  ______________
 *  |            |
 *  | GAME  CARD |
 *  |____________|
 *
 *    Start Game ->(game option screen)
 *
 */

/* Game option screen:
 *
 * >Begin!!! -> Begin game
 *  Ship Select   ->(Char select screen)
 *  Game difficulty ->(game difficulty screen)
 *
 *  Codes ->(cheat code screen)
 *
 */

/* Char select screen:
 *
 * >shipA   shipB   shipC
 *   ______
 *   |    |  ship ability
 *   |____|  description
 */

/* Game difficulty screen:
 *
 *  Explanation on difficulty
 *       > Easy
 *         Normal
 *         Hard
 */

/* Cheat Code screen:
 *
 *  cheat codes
 *   0 0 0 0 0 0
 * (use < and > to select the number to modify,
 *  use ^ and v to change value of currently selected number,
 *  currently selected number is highlited by having uparrow and downarrow)
 */

#include "mainmenu.h"

enum CheatType {
    ct_none,
    ct_chicken,
    ct_nocooldown,
    ct_invincibility,
    ct_morelives,
};

static void shipDescription(enum ShipType toComment);
static void testCheat(u32 cheat, GameState* gameMod);
static void drawCheat(u32 cheatCode);

void bootMenu(GameState* globalState)
{
    //Base block definitions for the menues screen map locations
    #define BG1_SBB  20
    #define CLEAN_SBB 14
    #define BOOT_SBB 15
    #define MAIN_SBB 16
    #define SHIP_SBB 17
    #define DIFF_SBB 18
    #define CODE_SBB 19

    //Title card screen
    volatile bgcnt* bg0cnt =  (bgcnt*) 0x4000008; //REG_BG0CNT
    volatile bgcnt* bg1cnt =  (bgcnt*) 0x400000A; //REG_BG1CNT

    bg0cnt->paletteType = 1;
    bg0cnt->priority = 1;
    bg0cnt->screenBaseBlock = BOOT_SBB;

    bg1cnt->paletteType = 1;
    bg1cnt->priority = 2;
    bg1cnt->screenBaseBlock = CLEAN_SBB;

    DMAcopy((void*)TILE_SHEET, (void*)menuset_til_bin, 12288, DMA16NOW);
    DMAcopy((void*)BACKGROUND_PALETTE, (void*)menuset_pal_bin, 256, DMA16NOW);

    drawText("Press Start", 10, 16, BOOT_SBB);
    drawPic(96, 17, 9, 7, 2, BOOT_SBB);

    //Title card loop
    int frameCount = 0;
	for (;;) {
        frameCount = (frameCount+1) % (BLANK_SEC<<1);
        if (frameCount == BLANK_SEC){
            drawText("           ", 10, 16, BOOT_SBB);
		} else if (frameCount == 0) {
            drawText("Press Start", 10, 16, BOOT_SBB);
		}

        if ((frameCount % (BLANK_SEC>>2)) == 0) {
            u16 temp = BACKGROUND_PALETTE[34];
            BACKGROUND_PALETTE[34] = BACKGROUND_PALETTE[35];
            BACKGROUND_PALETTE[35] = BACKGROUND_PALETTE[36];
            BACKGROUND_PALETTE[36] = temp;
        }
        if (CHECK_INPUT(KEY_START)) {
            srand(frameCount);
            goto gameMenu;
        }
        WAIT_BLANK;
    }

gameMenu:
{
    #define BEGIN_ROW 4
    #define SHIP_ROW 7
    #define CHEAT_ROW 10

    #define FIRST_ROW 4
    #define LAST_ROW 10


    bg0cnt->screenBaseBlock = MAIN_SBB;
    bg1cnt->screenBaseBlock = CLEAN_SBB;

    drawText("BEGIN GAME !!!", 7,  BEGIN_ROW, MAIN_SBB);
    drawText("Ship Select", 7,     SHIP_ROW,  MAIN_SBB);
    drawText("Code zone", 7,       CHEAT_ROW, MAIN_SBB);

    int currentRow = FIRST_ROW;
	u32 previousKey = KEY_START;
	for (;;) {
        frameCount = (frameCount+1) % BLANK_SEC;
        if (frameCount == 0 || frameCount == 4800) {
            drawText(">", 5, currentRow, MAIN_SBB);
		} else if (frameCount == 2400 || frameCount == 7200) {
            drawText(" ", 5, currentRow,MAIN_SBB);
		}

        u32 curKeys = ~REG_KEYVIEW;
        if (previousKey != curKeys) {
            if (TST_FG(curKeys,KEY_A)) { //Changes submenu.
                drawText(" ", 5, currentRow, MAIN_SBB);
                switch (currentRow)
                {
                    //Exits the game menu loop
                    case 0:
                        break;
                    case BEGIN_ROW:
                        return; //goes to game
                    case SHIP_ROW:
                        goto shipMenu;
                    case CHEAT_ROW:
                        goto cheatMenu;
                }
            } else if (TST_FG(curKeys, KEY_UP)) {
                drawText(" ", 5, currentRow, MAIN_SBB);
                currentRow = (currentRow == FIRST_ROW)? LAST_ROW :
                                (currentRow - 3);
                drawText(">", 5, currentRow, MAIN_SBB);
            } else if (TST_FG(curKeys, KEY_DOWN)) {
                drawText(" ", 5, currentRow, MAIN_SBB);
                currentRow = (currentRow == LAST_ROW)? FIRST_ROW :
                                (currentRow + 3);
                drawText(">", 5, currentRow, MAIN_SBB);
            }
			previousKey = curKeys;
        }
        WAIT_BLANK;
    }
}

shipMenu:
{
    #define BLANK 1
    #define SPEAR 11
    #define PALADIN 20

    #define FIRST_SHIP BLANK
    #define LAST_SHIP PALADIN

    bg0cnt->screenBaseBlock = SHIP_SBB;

    drawText("Select your ship:", 4, 1,SHIP_SBB);

    drawText("Blank", 2, 3, SHIP_SBB);
    drawText("Spear", 12, 3, SHIP_SBB);
    drawText("Paladin", 21, 3, SHIP_SBB);
    drawText("Current ship:", 1,7,SHIP_SBB);
    enum ShipType curShip = globalState->playerShip;
    int curPos = 1;

    drawText(">", curPos, 3, SHIP_SBB);
    drawPic((113 + ((int)curShip)*4),  3, 3, 1, 9, SHIP_SBB);
    shipDescription(curShip);

	u32 previousKey = KEY_A;
	for (;;) {
        frameCount = (frameCount+1) % BLANK_SEC;
        if (frameCount == 0 || frameCount == 4800) {
            drawText(">", curPos, 3, SHIP_SBB);
		} else if (frameCount == 2400 || frameCount == 7200) {
            drawText(" ", curPos, 3, SHIP_SBB);
		}

		u32 curKeys = ~REG_KEYVIEW;
        if (curKeys != previousKey) {
            if (TST_FG(curKeys, KEY_B)) { //Goes back to game menu
                drawText(" ", curPos, 3, SHIP_SBB);
                goto gameMenu;
            } else if (TST_FG(curKeys, KEY_A)) {
                switch(curPos)
                {
                case BLANK:
                    globalState->playerShip = st_Blank;
                    curShip = st_Blank;
                    break;

                case SPEAR:
                    curShip = st_Spear;
                    globalState->playerShip = st_Spear;
                    break;

                case PALADIN:
                    curShip = st_Paladin;
                    globalState->playerShip = st_Paladin;
                    break;
                }
                drawPic((113 + ((int)curShip)*4),  3, 3, 1, 9, SHIP_SBB);
                shipDescription(curShip);

            } else if (TST_FG(curKeys, KEY_LEFT)) {
                drawText(" ", curPos, 3, SHIP_SBB);
                switch (curPos)
                {
                case BLANK:
                    curPos = PALADIN;
                    break;
                case SPEAR:
                    curPos = BLANK;
                    break;
                case PALADIN:
                    curPos = SPEAR;
                    break;
                }
                drawText(">", curPos, 3, SHIP_SBB);
            } else if (TST_FG(curKeys, KEY_RIGHT)) {
                drawText(" ", curPos, 3, SHIP_SBB);
                switch (curPos)
                {
                case BLANK:
                    curPos = SPEAR;
                    break;
                case SPEAR:
                    curPos = PALADIN;
                    break;
                case PALADIN:
                    curPos = BLANK;
                    break;
                }
                drawText(">", curPos, 3, SHIP_SBB);
            }
			previousKey = curKeys;
        }
        WAIT_BLANK;
    }

}

cheatMenu:
{
    #define POINTER_LOC (((5-curNum) * 2) + 6)

    #define DRAW_POINTER \
        drawMos(157,POINTER_LOC,3,1,1,CODE_SBB);\
        drawMos(158,POINTER_LOC,5,1,1,CODE_SBB)

    #define UNDRAW_POINTER \
        drawText(" ",POINTER_LOC, 3,CODE_SBB);\
        drawText(" ",POINTER_LOC, 5,CODE_SBB)


    static u32 curCheat;
    u32 curNum = 5;

    frameCount = BLANK_SEC;

    bg0cnt->screenBaseBlock = CODE_SBB;

    bg1cnt->priority = 0; //We want to use bg1 as a mask for blinking cheat desc
    bg1cnt->screenBaseBlock = BG1_SBB;

    drawText("Enter cheat Code:", 2, 2, CODE_SBB);
    DRAW_POINTER;
    drawCheat(curCheat);

	u32 previousKey = KEY_A;
    for (;;) {
		u32 curKeys = ~REG_KEYVIEW;
        if (curKeys != previousKey) {
            if (TST_FG(curKeys, KEY_B)) { //Goes back to game menu
                UNDRAW_POINTER;
                goto gameMenu;
            } else if (TST_FG(curKeys, KEY_A)) {
                drawMos(0, 4, 6, 24, 2, CODE_SBB);
                for (int i = 0; i < 640; i++)
                    WAIT_BLANK;
                testCheat(curCheat, globalState);

            } else if (TST_FG(curKeys, KEY_LEFT)) {
                UNDRAW_POINTER;
                curNum = (curNum + 1) % 6;
                DRAW_POINTER;

            } else if (TST_FG(curKeys, KEY_RIGHT)) {
                UNDRAW_POINTER;
                curNum = (curNum == 0)? 5 : (curNum - 1);
                DRAW_POINTER;

            } else if (TST_FG(curKeys, KEY_UP)) {
                //In insight, I should have used bit fields for this one
                curCheat=(curCheat & ~(0xF << (4*curNum))) |
                        ((((curCheat >> (4*curNum)) + 1) & 0xF) << (4*curNum));
                drawCheat(curCheat);
            } else if (TST_FG(curKeys, KEY_DOWN)) {
            //equivalent to curCheat[curNum]-- with size(curCheat) = 4 bit.
                curCheat=(curCheat & ~(0xF << (4*curNum))) |
                        ((((curCheat >> (4*curNum)) - 1) & 0xF) << (4*curNum));
                drawCheat(curCheat);
            }
			previousKey = curKeys;
        }
        WAIT_BLANK;
    }
}

}


static void shipDescription(enum ShipType toComment)
{
    switch(toComment)
    {
    case st_Blank:
        drawText("Good all around. Has   ", 5, 9, SHIP_SBB);
        drawText("the power to banish    ", 5, 10, SHIP_SBB);
        drawText("bullets in a blink     ", 5, 11, SHIP_SBB);
        break;

    case st_Spear:
        drawText("A very powerfull ship  ", 5, 9, SHIP_SBB);
        drawText("favors offense at the  ", 5, 10, SHIP_SBB);
        drawText("expense of defense     ", 5, 11, SHIP_SBB);
        break;

    case st_Paladin:
        drawText("This ship was built to ", 5, 9, SHIP_SBB);
        drawText("last. Has the ability  ", 5, 10, SHIP_SBB);
        drawText("to convert bullets     ", 5, 11, SHIP_SBB);
        break;
    }
}


/* Cheat codes:
 * 4B4643 transforms your ship into a chicken
 * 2B5215 gives you infinite B button use
 * 76031B gives you invulnerability
 * AEDFXX last 2 numbers is your ammount of lives
 *
 * extra cheat code candidates: Pickup explodes on touch
 */
static void testCheat(u32 cheatCode, GameState* gameState)
{
    if (cheatCode == 0x4B4643) {
    //chicken cheat
        gameState->chickenCheat = 1;
        drawText("Now you are the chicken!", 4, 6, CODE_SBB);
        drawText("                        ", 4, 7, CODE_SBB);
    } else if (cheatCode == 0x2B5215) {
    //no special cooldown
        gameState->infBCheat = 1;
        drawText("No restriction fun!     ", 4, 6, CODE_SBB);
        drawText("No cooldown B special.  ", 4, 7, CODE_SBB);
    } else if (cheatCode == 0x76031B) {
    //Invincibility
        gameState->invulCheat = 1;
        drawText("Immortal...             ", 4, 6, CODE_SBB);
        drawText("Now this is a real cheat", 4, 7, CODE_SBB);
    } else if ((cheatCode & 0xFFFF00) == 0xAEDF00) {
    //lives count (not implemented yet)
        drawText("Todo: implement extra   ", 4, 6, CODE_SBB);
        drawText("lives cheat...          ", 4, 7, CODE_SBB);
    } else {
        drawText(">Invalid cheat phrase!< ", 4, 6, CODE_SBB);
        drawText("                        ", 4, 7, CODE_SBB);
    }
}


static void drawCheat(u32 cheatCode)
{
    //we display the cheat code in base 16
    for (int i = 16; i >4; i-=2, cheatCode >>= 4)
        BG_TILEMAP(CODE_SBB)[i + 128].xcoord = NUM_TILE(cheatCode & 0xF);
        //128 = 32 * 4 (4 is the tile y position)
}
