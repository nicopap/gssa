#ifndef WEAPONS_H
#define WEAPONS_H

#include "gba_def.h"

#include "player.h"
#include "bullets.h"

//Extra weapon ideas:
// -  a slow firing bullet that is very powerful
// -  momentum gun slows down as it levels up
// -  std gun increase in speed and firerate with levels
// -  double is more precise, faster and do more damage
//      with levels
// - A shotgun type weapon that shoots many bullets in short range
//   . shots several bullets with different sprites that represents several
//      small bullets, to improve illusion of having many bullets
void shootWeapon(PlayerShip*, u32 playerInput);

#endif //WEAPONS_H