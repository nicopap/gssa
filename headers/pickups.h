#ifndef PICKUP_DEF_H
#define PICKUP_DEF_H

#include <stdlib.h> //rand()

#include "gba_def.h" //u8
#include "gba_util.h" //oamClone
#include "gamestate.h" //GameState
#include "player.h" //PlayerShip

// Extra ideas:
// - drop rate must change depending on which ship is played

#define MAX_PICKUP 8

enum pickupType {
	pt_null = 0,
	pt_fillAbil = 1,
	pt_weaponUp = 2,
	pt_stdWp	= 3,
	pt_momentumWp  = 4,
	pt_chargeWp  = 5,
	pt_doubleWp = 6,
	pt_lifeUp   = 7
};

typedef struct s_active_pickup {
	enum pickupType type;
	int xpos;
	int ypos;
	u8 sindex;
} ActivePickup;

void initPickup(GameState*, ActivePickup* activePickupArray);

void addPickup(enum pickupType, int xpos, int ypos);
void addRandPickup(int xpos, int ypos);
void collectPickup(ActivePickup*, PlayerShip*);
void pickupHandler();
int actvPickupCount();

#endif // PICKUP_DEF_H
