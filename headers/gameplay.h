#ifndef GAMEPLAY_H
#define GAMEPLAY_H

#include "gba_def.h"
#include "gba_util.h"

#include "gamestate.h"

#include "player.h"
#include "waves.h"
#include "ennemies.h"
#include "bullets.h"
#include "pickups.h"
#include "game_graphics.h"
#include "game_screens.h"

void playGame(GameState* settings);

void collisionHandler();
int waveHandler(int waveList[], int waveListSize);

#endif //GAMEPLAY_H