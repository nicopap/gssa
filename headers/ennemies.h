#ifndef ennemies_H
#define ennemies_H

#include <stdlib.h> //rand(), abs()

#include "gba_def.h"
#include "gba_util.h" // spriteTag* oamClone

#include "bullets.h"
#include "pickups.h"
#include "player.h"

#define MAX_ENNEMY 31


typedef struct s_active_ennemy {
	u8 ennemyID; //ennemyID 0 indicates no ennemies
	u8 sindex;
	u16 spriteIndex;
	int xpos;
	int ypos;
	signed short hspeed;
	signed short vspeed;
	signed short hp;
	u16 hitAnimCycle;
	int AIcycle; //Those two  values are expected to be set to 0
	int AIextraData; //  !! TODO !! : set those to an union
	int AIcannonCycle;
} ActiveEnnemy;

struct Box {
	u8 x;
	u8 y;
	u8 l;
	u8 h;
}; //Ennemy hitbox position

typedef struct s_boxDes {
	const struct Box* boxList;
	const int boxCount;
} BoxDes; // Ennemy hitbox description

struct CannonPos {
	u8 x;
	u8 y;
}; // Ennemy cannons position on sprite

typedef struct s_cannonDes {
	struct CannonPos* cannonList;
	int cannonCount;
} CannonDes; // Ennemy cannons position

typedef struct s_ennemy {
	const u8* sprite;
	enum atr_ss spriteSize  : 3;
	enum atr_sh shape : 3;
	BoxDes* hitbox;
	CannonDes* cannons;
	signed short startHP;
	u16 projectile : 10; //Only an indication,
	  // the AI function can override the bullet type
	void (*AIfunction)(ActiveEnnemy*, PlayerShip*); //AI function
} Ennemy;

extern const Ennemy ennemySheet[];

void initEnnemies(ActiveEnnemy osEnm[]);

void ennemyHandler(PlayerShip*); //displays ennemies, runs their AI
  // And checks if player is in contact with an ennemy ship

void ennemyHit(ActiveEnnemy* hitEnm, PlayerShip*);
void killEnnemy(int ennemyNbr);
void addEnnemy(ActiveEnnemy*);

u8  actvEnnemyCount(); //Returns number of alive ennemies on screen
int inHitBox(ActiveEnnemy*, int xpos, int ypos);

#endif //ennemies_H
