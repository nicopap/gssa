#ifndef MAINMENU_H
#define MAINMENU_H

#include <stdlib.h>

#include "gba_def.h"
#include "gba_util.h"
#include "gamestate.h"
#include "bgutil.h"

#include "menuset_pal_bin.h"
#include "menuset_til_bin.h"

/* Here are all the operations done during the menu loop
 * from the boot to the start of the game loop.
 * it will change the gamestate variables according to options given.
 */

#define BLANK_SEC 9600 //Is the number of v-blanks in a second (160*60)

void bootMenu(GameState* globalGameState);

#endif //MAINMENU_H
