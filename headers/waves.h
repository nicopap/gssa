#ifndef waves_H
#define waves_H

#include "gba_def.h"
#include "gba_util.h"
#include "ennemies.h"

extern volatile int WAVE_COMPLETION_FLAG;
#define WCF_INCOURSE 1
#define WCF_OVER 0


typedef struct s_wave_entry {
	u8 ennemyID; //Which ennemy is spawned
	signed short hspeed;
	signed short vspeed;
	int xpos;
	int ypos;
	signed char holdTime;
} WaveEntry;


typedef struct s_wave {
	int waveSize; //is the ammount of ennemies in this wave
	const WaveEntry* enList;
} Wave;

extern const Wave waveSheet[]; //List of waves

void spawnWave(int wave);
void setupWAVEhandler();
int currentWaveEnnemyCount();
void testWaveTime();

void endWave();

#endif //waves_H
