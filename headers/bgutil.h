#ifndef BGUTIL_H
#define BGUTIL_H

#include "gba_def.h"

#define CHAR_TILE(/*char*/x) ((x)-32) //ASCII --> MENUSET
#define NUM_TILE(x) (((x)<10)? ((x)+16) : ((x)+23))

//draws text starting at tile xpos,ypos
void drawText(char* text, u32 xpos, u32 ypos, u32 mapLoc);

//draws sprite of width and heigh w,h at tile xpos, ypos
void drawPic(u32 spriteLoc, u32 w, u32 h, u32 xpos, u32 ypos, u32 mapLoc);

//draws a square starting at xpos,ypos of size w x h with the tile tile
void drawMos(u32 tile, u32 xpos, u32 ypos, u32 w, u32 h, u32 mapLoc);

//Cycles colors in the palette starting at startColor and ending at endColor
void cyclePalette(u32 startColor, u32 endColor);

#endif // BGUTIL_H