#ifndef COMPACT_ARRAY_H
#define COMPACT_ARRAY_H


#define insertCompArray(array,elemCnt,elem) array[elemCnt++] = elem

//Possible improvements: use DMA to move array content
#define extractCompArray(array,elemCnt,rm) array[rm] = array[--elemCnt]
#endif //COMPACT_ARRAY_H
