#ifndef bullets_H
#define bullets_H

#include "gba_def.h"
#include "gba_util.h"

#include<stdlib.h> //abs(int)

// Extra ideas:
// - make bullet movement ID dependant.

#define MAX_BULLET 88 // WARNING: must be multiple of 8 for optimisation
#define BULLET_COUNT 14 //total number of bullets

struct bulletPoints {
	signed short x;
	signed short y;
};

typedef struct s_bulletData {
	const struct bulletPoints* hitPoints;
	int hitPcount;
	//void (*bulletAI)(ActiveBullet*, PlayerShip*);
} bulletData;


struct st_ActiveBullet {
	i32 xpos;
	i32 ypos;
	i16 hspeed;
	i16 vspeed;
	u16 bulletID; //finds the sprite
	u8 owner; //tells if player bullet or ennemy bullet
	u8 sindex;
	//u16 bulletPower;
};
typedef struct st_ActiveBullet ActiveBullet;

void initBullets(ActiveBullet actvBulletList[]); //Sets all values of memory
// locations for the bullet handling routines

void bulletHandler();

void killBullet(int index);
void addBullet(ActiveBullet*);
int actvBulletCount();

#endif // bullets_H
