#ifndef MENUTIL_H
#define MENUTIL_H

#include "gba_def.h"

struct menuentry {
	char* text;
	int xloc;
	int yloc;
	void (*actionFunction)(); //Action to execute when menu entry is choosen
};

enum direction {
	DIR_left,
	DIR_up,
	DIR_right,
	DIR_down
};

struct menuui { //Keeps track of screen mode and menu itself
	struct menuentry* entries; //list of menuentries
	u8 entryCount;
	u8 currentEntry;
	void (*switchAction)(struct menuui* this, enum direction dir);
	//Routine to execute. When switching active entry. If null,
	//the default action of switching entry is made.
	void (*cancelAction)(struct menuui* this);
};

struct video_menuui {
	struct menuui* menuui;
	u8 videoMode : 3;
	u8 videoBuffer : 5;
	void (*drawmenu)(); //draws the menu for first invocation
	void (*undrawmenu)();
};

struct menuui* initMenuui(
		struct menuentry[],
		u8 entryCount,
		void (*switchAction)(struct menuui*, enum direction),
		void (*cancelAction)(struct menuui*)
);

void menuHandle(struct video_menuui* menu); //Goes into loop strictly handled by menu itself


#endif //MENUTIL_H
