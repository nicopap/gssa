#ifndef GAME_GRAPHICS_H
#define GAME_GRAPHICS_H

#include <stdlib.h> //rand()

#include "gba_def.h" //video ram locations
#include "gba_util.h" //DMAcopy()
#include "bgutil.h"

#define PLAYER_BULLET_POS 26

#define UI_SBB 22

void loadGraphics();
void cycleGraphics(int scrollSpeed);

#endif //GAME_GRAPHICS_H