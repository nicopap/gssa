#ifndef GBA_DEF_H
#define GBA_DEF_H

#include <stdint.h>

//GBA screen resolution: 240x160

#define NULL 0xdeadbeef
typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;

typedef struct s_dispstatcnt {
    u16 vBlankStat : 1;
    u16 hBlankStat : 1;
    u16 vCountStat : 1;
    u16 vBlankIntrpt : 1;
    u16 hBlankIntrpt : 1;
    u16 vCountIntrpt : 1;
    u16 undef1 : 2;
    u16 vCountSetup : 8;
} dispStatCnt;
#define DISPSTAT_CNT ((volatile dispStatCnt*) 0x4000004)

typedef struct s_bgcnt {
    u16 priority : 2;
    u16 charBase : 2;
    u16 emp2     : 2;
    u16 mosaic   : 1;
    u16 paletteType     : 1;
    u16 screenBaseBlock : 5;
    u16 overflow     : 1;
    u16 size     : 2;
} bgcnt;

struct s_tileVal {
    u16 xcoord  : 10;
    u16 hflip   : 1;
    u16 vflip   : 1;
    u16 undef   : 4;
};
typedef struct s_tileVal tileVal;

enum atr_col {
    ATR_256COL = 1,
    ATR_16COL = 0,
};

enum atr_sh {
    ATR_SQUARE = 0,
    ATR_TALL = 1,
    ATR_WIDE = 2,
};

enum atr_ss {
    ATR_SIZE8  = 0,
    ATR_SIZE16 = 1,
    ATR_SIZE32 = 2,
    ATR_SIZE64 = 3
};

typedef struct s_spriteTag {
//attribute0
    u16 ycoord     : 8;
    u16 rotFlag    : 1;
    u16 doubleFlag : 1;
    u16 transpFlag : 1;
    u16 windFlag   : 1;
    u16 mosaicFlag : 1;
    enum atr_col colorType : 1;
    enum atr_sh shape      : 2;
//attribute1
    u16 xcoord : 8;
    u16 undef1 : 4;
    u16 hflip  : 1;
    u16 vflip  : 1;
    enum atr_ss spriteSize : 2;
//attribute2
    u16 spriteLoc : 10;
    u16 priority  : 2;
    u16 palette   : 4;
//attribute3
    u16 undef2 : 16;
} spriteTag;


#define VCOUNT_DISP_REG (*(volatile u16*) 0x20)

//BACKGROUND MANIPULATION DEFINITIONS
#define REG_BG0CNT 0x4000008
#define REG_BG2CNT 0x400000C
#define REG_BG3CNT 0x400000E

#define BG0VOFS 0x4000012
#define BG2HOFS 0x4000018
#define BG2X_L  0x4000028

//DEFINITION BACKGROUND
#define BG0_ENABLE 	0x100
#define BG1_ENABLE	0x200
#define BG2_ENABLE	0x400
#define BG3_ENABLE	0x800


#define SPRITE_PALETTE     ((u16*)0x5000200)
#define SPRITE_MEMORY      ((u16*)0x07000000)
#define SPRITE_DATA        ((u16*)0x6010000)
#define BACKGROUND_PALETTE ((volatile u16*)0x5000000)
#define TILE_SHEET         ((u16*)0x6000000)
#define TILE_SHEET_BK1     ((u16*)0x6004000)
#define BG_TILEMAP(x)     ((tileVal*)(TILE_SHEET + ( x << 10)))
#define BG_TILESMALL(x) (TILE_SHEET + ( x << 10))
#define TILESETU8(x) (((u8*)0x6000000) + ((x) << 11))


//DEFINITION DMA
#define REG_DMA3SAD (*(volatile u32*)0x40000D4)
#define REG_DMA3DAD	(*(volatile u32*)0x40000D8)
#define REG_DMA3CNT_L	(*(volatile u16*)0x40000DC)
#define REG_DMA3CNT_H	(*(volatile u16*)0x40000DE)

#define DMA_ENABLE	0x8000
#define DMA_TIMING_IMMEDIATE	0x0000
#define DMA_TIMING_NXTVBLANK 0x1000
#define DMA_16		0x0000
#define DMA_32		0x0400
#define DMA16NOW	(DMA_ENABLE | DMA_TIMING_IMMEDIATE | DMA_16)
#define DMA32NOW	(DMA_ENABLE | DMA_TIMING_IMMEDIATE | DMA_32)

#define DMA16NVB	(DMA_ENABLE | DMA_TIMING_NXTVBLANK | DMA_16)
#define DMA32NVB	(DMA_ENABLE | DMA_TIMING_NXTVBLANK | DMA_32)

//DEFINITION REGISTRES
#define REG_INTRPT	(*(u32*)0x3007FFC)
#define REG_DISPCNT	(*(volatile u16*)0x4000000)
#define REG_DISPSTAT	(*(volatile u16*)0x4000004)
#define REG_VBLANK	(*(volatile u16*)0x4000006)
#define REG_KEYCNT 	(*(volatile u16*)0x4000132)
#define REG_KEYVIEW (*(volatile u16*)0x4000130)
#define REG_IE		(*(volatile u16*)0x4000200)
#define REG_IF		(*(volatile u16*)0x4000202)
#define REG_IME		(*(volatile u16*)0x4000208)

#define SetMode(mode) 	REG_DISPCNT = (mode)

#define RGB(r,g,b) (u16)((((r) & 31) << 1) + (((g) & 31) << 6) + (((b) & 31) << 11))

//DEFINITION ELEMENTS GRAPHIQUES
#define OBJ_MAP_1D 	0x40
#define OBJ_ENABLE 	0x1000

//DEFINITION VIDEO MODES
#define VID_MODE_0 0x0
#define VID_MODE_1 0x1
#define VID_MODE_2 0x2
#define VID_MODE_3 0x3
#define VID_MODE_4 0x4
#define VID_MODE_5 0x5

//DEFINITION INTERRUPTS REG_IE
#define INT_VBLANK 	0x0001
#define INT_HBLANK 	0x0002
#define INT_VCOUNT 	0x0004
#define INT_TIMER0 	0x0008
#define INT_TIMER1 	0x0010
#define INT_TIMER2 	0x0020
#define INT_TIMER3 	0x0040
#define INT_COM 	0x0080
#define INT_DMA0 	0x0100
#define INT_DMA1 	0x0200
#define INT_DMA2 	0x0400
#define INT_DMA3 	0x0800
#define INT_BUTTON 	0x1000
#define INT_CART 	0x2000

//DEFINITION TIMERS
#define TIMER_OVERFLOW 0x4
#define TIMER_IRQ_ENABLE 0x40
#define TIMER_ENABLE 0x80

#define TIMER_FREQUENCY_SYSTEM	0x0
#define TIMER_FREQUENCY_64	0x1
#define TIMER_FREQUENCY_256	0x2
#define TIMER_FREQUENCY_1024	0x3

#define REG_TM0CNT (*(volatile u16*)0x4000102)
#define REG_TM1CNT (*(volatile u16*)0x4000106)
#define REG_TM2CNT (*(volatile u16*)0x400010A)
#define REG_TM3CNT (*(volatile u16*)0x400010E)

#define REG_TM0D (*(volatile u16*)0x4000100)
#define REG_TM1D (*(volatile u16*)0x4000104)
#define REG_TM2D (*(volatile u16*)0x4000108)
#define REG_TM3D (*(volatile u16*)0x400010C)

//DEFINITION BOUTONS GBA
#define KEY_A 		0x1
#define KEY_B 		0x2
#define KEY_SELECT 	0x4
#define KEY_START 	0x8
#define KEY_RIGHT 	0x10
#define KEY_LEFT    0x20
#define KEY_UP 		0x40
#define KEY_DOWN 	0x80
#define KEY_R 		0x100
#define KEY_L 		0x200

#endif //GBA_DEF_H
