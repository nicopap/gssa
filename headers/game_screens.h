#ifndef GAME_SCREENS_H
#define GAME_SCREENS_H

#include "gamestate.h"
#include "gba_def.h"
#include "game_graphics.h"//UI_SBB (location of tile mapping area)
#include "menutil.h"

void screenGameOver();
void screenLevelComplete();
void screenPause();

#endif //GAME_SCREENS_H