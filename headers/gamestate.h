#ifndef GAMESTATE_H
#define GAMESTATE_H

#include "gba_def.h"

enum Screen {
	s_ingame,
	s_paused,
	s_gameover,
	s_win,
};

enum ShipType {
	st_Blank,
	st_Spear,
	st_Paladin,
};

// Refers to ship type for alt weapon.
#define AltWeapon ShipType
  #define aw_altA st_Blank
  #define aw_altB st_Spear
  #define aw_altC st_Paladin

enum Difficulty {
	d_easy,
	d_normal,
	d_hard,
};


typedef struct s_gamestatetype {
	enum ShipType playerShip : 8;
	enum Difficulty difficulty : 5;
	u8 chickenCheat : 1;
	u8 infBCheat : 1;
	u8 invulCheat : 1;
	u8 curLevel : 8;
	enum  Screen curScreen : 8;
} GameState;

#endif //GAMESTATE_H
