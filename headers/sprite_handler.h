#ifndef SPRITE_HANDLER_H
#define SPRITE_HANDLER_H

#include "gba_def.h"
#include "gba_util.h"

#define X_TO_SPRITE_SIZE(x,S) x = 1 << (2 * (int) (S).spriteSize);\
	if ((S).shape != ATR_SQUARE) x *= 2

void initSprites();

int allocSprite(const u8* src, int spriteLength); //Returns index in sprite mem
  //Of the allocated sprite
void changeSprite(int dst, const u8* src, int spriteLength); //replaces
  // sprite at dst with sprite at src of size spriteLength
void freeSprite(int indx, int spriteLength);

extern signed char sindexStack[];
extern short sindexStart;

#define getSindex(valToSet) \
{\
	extern signed char sindexStack[];\
	extern short sindexStart;\
	int ret = sindexStack[sindexStart];\
	if (ret == -1)\
		blueScreen("Found a -1 in the sindex stack");\
	sindexStack[sindexStart++] = -1;\
	valToSet = ret;\
}

#define giveSindex(returned) \
{\
	extern signed char sindexStack[];\
	extern short sindexStart;\
	if (sindexStack[--sindexStart] != -1)\
		blueScreen("Found a non -1 value in the sindex stack "\
				   "while freeing a spot");\
	sindexStack[sindexStart] = returned;\
}

#endif //SPRITE_HANDLER_H
