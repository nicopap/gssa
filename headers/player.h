#ifndef PLAYER_H
#define PLAYER_H

#include "gba_def.h"
#include "gba_util.h" // spriteTag* oamClone;
#include "bullets.h"
#include "gamestate.h"
#include "game_screens.h"

#define FILLED_ABILITY 5

#define PLAYER_SPEED 256
#define PLAYER_INV_FRAMES 150


enum Weapon {
	w_standard,
	w_double,
	w_momentum,
	w_charge,
};

enum WeaponMod {
	wm_default = 0, //Green color
	wm_up1 = 1, //Blue color
	wm_up2 = 2, //Red color
	wm_up3 = 3, //Yellow color
};

typedef struct s_playership {
	int xpos; //player position
	int ypos;
	int cooldown; //shooty weapon cooldown
	int abilAval; //ability stats
	int iFrames; //invincibility time remaining
	u8 remainingLives;
	enum Weapon curWeapon;
	enum WeaponMod curMod;
} PlayerShip;

void initPlayer(PlayerShip*, GameState*);
void displayPlayer(PlayerShip*);

void playerInputHandler(PlayerShip*);
void playerHit(PlayerShip*);
int playerDamage(PlayerShip*); //Returns dmg done by bullet shot by ps
void playerAbility(PlayerShip*);

void weaponDown(PlayerShip*);
void weaponUp(PlayerShip* ps);


#endif //PLAYER_H
