#ifndef EXIT_DEBUG_H
#define EXIT_DEBUG_H

#include "gba_def.h"
#include "gba_util.h"
#include "bgutil.h"

void blueScreen(char* errorMessage);

#endif // EXIT_DEBUG_H