#ifndef GBA_UTIL_H
#define GBA_UTIL_H

#include <string.h>

#include "gba_def.h"

#define WAIT_BLANK while((REG_DISPSTAT & INT_VBLANK)!= INT_VBLANK)
#define FREEZ_FRAME REG_IME = 0;\
    for(int i = 0; i < 160; i++)\
        while((REG_DISPSTAT & INT_VBLANK)!= INT_VBLANK);\
    REG_IME = 1

#define WAIT_SEMI_FRAME {int cnStat;\
        do { cnStat = REG_VBLANK;\
        }while(cnStat != 40 && cnStat != 120 ); }

#define WAIT_INPUT(key) while((REG_KEYVIEW & key) != key)
#define CHECK_INPUT(key) ((~REG_KEYVIEW & key) == key)
#define TST_FG(field,flag) ((field & flag) == flag)

spriteTag oamClone[128] ;

void DMAcopy(void* dst, const void* src, u16 size, u16 type);

void vcountHandler();
void interHandler();
void setupOAMinterrupt();
void setupOAM();

#endif //GBA_UTIL_H
