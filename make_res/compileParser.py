import os
import sys

os.chdir(sys.argv[1])

buildScript = "resCompile.txt"
if len(sys.argv) > 2: buildScript = sys.argv[2]

cmplFile = open(buildScript, 'r')

def exitWithError(msg = "Syntax error"):
    print(msg)
    sys.exit()

def parseLine(toParse, curCtx):
    """Returns context updated with toParse a line to parse"""
    striped = toParse.strip()
    if striped.startswith("#"):
        curCtx["mustConvrt"] = False
    elif striped == '':
        curCtx["mustConvrt"] = False
    elif striped == "palettes:":
        print("Creating palettes:")
        curCtx["convType"] = "palettes"
        curCtx["mustConvrt"] = False
    elif striped == "tiles:":
        print("Creating tiles:")
        curCtx["convType"] = "tiles"
        curCtx["mustConvrt"] = False
    elif striped.startswith("in ") and striped.endswith(":"):
        curCtx["curSubd"] = striped[3:len(striped)-1]
        curCtx["mustConvrt"] = False
    else:
        curCtx["mustConvrt"] = True
    return curCtx

def paletteLine(toRead,curCtx):
    """calls png2gbaLoc to compile inFile into outFile."""
    global png2gbaLoc
    global resIn
    global resOut

    striped = toRead.strip()
    inFile = os.path.join(resIn,
                          curCtx["curSubd"],
                          striped.split(">>")[0].strip())
    outFile = resOut + "/" + striped.split(">>")[1].strip()
    if (not os.path.isfile(outFile+"_pal.bin")
      or (os.stat(inFile).st_mtime > os.stat(outFile+"_pal.bin").st_mtime)):
        os.system(' '.join((png2gbaLoc,inFile,outFile,"--palette")))

def tileLine(toRead,curCtx):
    """calls png2gbaLoc to compile inFile to outFile using the palFile
palette"""
    global png2gbaLoc
    global resIn
    global resOut

    striped = toRead.strip()
    inFile = resIn + "/" + curCtx["curSubd"] + "/" + \
               striped.split(">>")[0].strip()
    palFile = resIn + "/" + curCtx["curSubd"] + "/" + \
               striped.split(">>")[1].strip()
    outFile = resOut + "/" + striped.split(">>")[2].strip()
    if ((not os.path.isfile(outFile+"_til.bin"))
      or (os.stat(inFile).st_mtime > os.stat(outFile+"_til.bin").st_mtime)
      or (os.stat(palFile).st_mtime > os.stat(outFile+"_til.bin").st_mtime)) :
        os.system(
            ' '.join((png2gbaLoc, inFile, outFile, "--indexedTile8nop", palFile))
        )
#    else:
#        print(striped.split(">>")[2].strip() + " already up to date")

curLine = cmplFile.readline()
if not curLine.startswith("prog:"):
    exitWithError("Script should start with converter location (prog:<loc>)")
png2gbaLoc = curLine[5:len(curLine)-1]
print('Program Location: "' + png2gbaLoc + '"')

curLine = cmplFile.readline()
if not curLine.startswith("in:"):
    exitWithError("Second line should be the ressources to compile location")
resIn = curLine[3:len(curLine)-1]
print('Ressources Location:"' + resIn + '"')

curLine = cmplFile.readline()
if not curLine.startswith("out:"):
    exitWithError("Third line should be location of bins to output")
resOut = curLine[4:len(curLine)-1]
print('Output location: "' + resOut + '"')

curCtx = {"convType" : '', "curSubd" : '', "mustConvrt" : False}
for curLine in cmplFile:
    curCtx = parseLine(curLine,curCtx)
    if curCtx["mustConvrt"]:
        if curCtx["convType"] == "palettes":
            paletteLine(curLine,curCtx)
        elif curCtx["convType"] == "tiles":
            tileLine(curLine,curCtx)
        else:
            exitWithError()
