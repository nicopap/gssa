#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include "lodepng.h"

#define u8 uint8_t
#define u16 uint16_t

#define DEBUG 0

#define FILE_NAME_SIZE 100

#define STR_EQU(x,y) (strcmp(x,y)==0)

enum ConvertMode{
	palette,
	tileMap8,
	tileMap4,
	indexedTile8,
	indexedTile8no,
};

void turn16Bits(const u8* tripleChannel, u16* sixteenBit);

int genPalette(const u8* image, unsigned int h, unsigned int w,
			u8* gbaTiles, u16* gbaPalette, const u8* refPalette,
			unsigned int palSize);

void assPalette(u16* targetPicture, u8* tileSet, u16* pal,
				unsigned int w, unsigned int h);

int main(int argc, char *argv[])
{
	if ( argc < 3 || argc > 5 ||
	  !(STR_EQU(argv[3],"--tileMap8") ||
	    STR_EQU(argv[3],"--tileMap4") ||
	    STR_EQU(argv[3],"--palette")  ||
	    STR_EQU(argv[3],"--indexedTile8") ||
	    STR_EQU(argv[3],"--indexedTile8nop") ) )
	{
		if (argc > 1 && (STR_EQU(argv[1], "--help") ||
							STR_EQU(argv[1], "-h") ||
							STR_EQU(argv[1], "-help") ||
							STR_EQU(argv[1], "help")))
		{
			printf("use the program as follow:\n"
			"png2gba <input.png> <output> {"
			"--tileMap8 | --palette | --indexedTile8 <palette_filename.png>}\n"
			"\n"
			"--tileMap8:\n"
			"creates tile and palette files from input.png called "
			"output_til.bin and output_pal.bin respectively. _til.bin "
			"contains the converted tile mapped picture, _pal.bin is the "
			"The input "
			"file must have at most 256 different colors.\n"
			"\n"
			"--palette:\n"
			"takes input.png a less than 257 pixels png file and returns the "
			"palette file for the gba in output_pal.bin\n"
			"\n"
			"--indexedTile8 <palette.png>:\n"
			"creates output_til.bin and output_pal.bin the input.png "
			"file indexed by palette.png, all colors in input.png must "
			"be in palette.png\n"
			"\n"
			"--indexedTile8nop <palette.png>:\n"
			"same as --indexedTile8 <palette.png> but doesn't create a "
			"output_pal.bin\n"
			"\n"
			"All file names has to have less than 100 characters\n");
		}
		else
		{
			printf("wrong arguments: type png2gba -help for instrucitons\n");
		}
		exit(1);
	}
	if (strlen(argv[2]) > (FILE_NAME_SIZE - 9))
	{
		printf("Output file name length is too long,"
				"maximum is 95 characters\n");
		exit(1);
	}


	enum ConvertMode convType;
	if (argc == 3)
		convType = palette;
	else if (STR_EQU(argv[3], "--palette"))
		convType = palette;
	else if (STR_EQU(argv[3], "--tileMap8"))
		convType = tileMap8;
	else if (STR_EQU(argv[3], "--tileMap4"))
		convType = tileMap4;
	else if (STR_EQU(argv[3], "--indexedTile8"))
		convType = indexedTile8;
	else if (STR_EQU(argv[3], "--indexedTile8nop"))
		convType = indexedTile8no;
	else
	{
		printf("An unexpected issue happend (convertion type not found)\n");
		exit(1);
	}

	u8* decodeArray;
	unsigned int w,h, erret;

	printf("input file: %s\noutput file(s): %s\n",argv[1],argv[2]);
	erret = lodepng_decode_file(&decodeArray, &w, &h, argv[1], LCT_RGB, 8);
	if (erret)
	{
		printf("Lodepng error during first file decoding: %s\n",
				lodepng_error_text(erret));
		exit(1);
	}

	if (convType == palette)
	{
		u16* gbaArray = malloc(w * h * sizeof(u16)); //Array on which we write
		memset(gbaArray, 0, sizeof(u16) * w * h); //the palette value

		unsigned int y,x;
		for (y = 0; y < h; y++)
		for (x = 0; x < w; x++)
			turn16Bits(decodeArray + ((y*w + x)*3),
					   gbaArray+ (y*w + x));

		char* outputFile = malloc(FILE_NAME_SIZE); //file name
		strcpy(outputFile, argv[2]);
		strcat(outputFile, "_pal.bin");

		FILE* palette = fopen(outputFile, "wb");
		if (palette == NULL)
		{
			printf("Impossible to create %s\n", outputFile);
			exit(1);
		}
		fwrite(gbaArray, 2, w*h,palette);
		fclose(palette);
		printf("Palette succsefully created!\n");
		return 0;
	}
	if (convType == tileMap8 ||
	  convType == indexedTile8 ||
	  convType == indexedTile8no)
	{
		u8* refInput = NULL;
		unsigned int pal_w = 16;
		unsigned int pal_h = 16;

		if (convType == indexedTile8 || convType == indexedTile8no)
		//Creates reference palette if necessary
		{
			u8 er = lodepng_decode_file(&refInput, &pal_w, &pal_h, argv[4],
										LCT_RGB,8);
			if (er)
			{
				printf("Lodepng error during palette decoding: %s\n",
						lodepng_error_text(er));
				exit(1);
			}
		}
		u16* pltOutput = malloc(sizeof(u16) * pal_h * pal_w); //out palette
		memset(pltOutput, 0, sizeof(u16) * pal_h * pal_w);

		u8* tileOutput = malloc(w*h);
		memset(tileOutput, 0,w*h);

		int colorCount = genPalette(decodeArray, h, w, tileOutput, pltOutput,
									 refInput, pal_h * pal_w);

		if (colorCount >= pal_h * pal_w)
		{
			printf("Warning! The indexation might have failed due to the "
				   "color count exceeding the palette size.\n");
		}

		char* paletteFile = malloc(FILE_NAME_SIZE); //output palette name
		strcpy(paletteFile, argv[2]);
		strcat(paletteFile, "_pal.bin");

		if (convType != indexedTile8no)
		//We do not write the palette if user asked to not write it
		{
			FILE* paletteOutfile = fopen(paletteFile, "wb");
			if (paletteOutfile == NULL)
			{
				printf("impossible to create %s\n", paletteFile);
				exit(1);
			}
			fwrite(pltOutput, sizeof(u16), pal_h*pal_w, paletteOutfile);
			fclose(paletteOutfile);
		}


		char* tileFile = malloc(FILE_NAME_SIZE);
		strcpy(tileFile, argv[2]);
		strcat(tileFile, "_til.bin");

		#if DEBUG
			u16* retroing = malloc(2*w*h);
			assPalette(retroing, tileOutput, pltOutput,w,h);
			tileOutput = (u8*) retroing;
		#endif // DEBUG
		FILE* tileOutfile = fopen(tileFile, "wb");
		if (tileOutfile == NULL)
		{
			printf("impossible to create %s\n", tileFile);
			exit(1);
		}
		fwrite(tileOutput, 1, w*h, tileOutfile);
		fclose(tileOutfile);
		printf("Indexed tileset successfully indexed and saved!\n");
		return 0;
	}
	if (convType == tileMap4)
		printf("this feature is not implemented yet, sorry!\n");

	return 1;
}

// ------------------ //
// --- turn16Bits --- //
// ------------------ //
void turn16Bits(const u8* tripleChannel, u16* sixteenBit)
{
	u8 r = tripleChannel[0] >> 3; //put 8 bit color on 5 bit color
	u8 g = tripleChannel[1] >> 3;
	u8 b = tripleChannel[2] >> 3;

	*sixteenBit = r | (g << 5) | (b << 10);
}

// ------------------ //
// --- genPalette --- //
// ------------------ //
int genPalette(const u8* image, unsigned int h, unsigned int w,
			u8* gbaTiles, u16* gbaPalette, const u8* refPalette,
			unsigned int palSize)
/* returns count of colors in the picture image.
 * creates a bytearray where each byte is an indexed color
 * gbaTile is the bytearray
 * gbaPalette is the palette corresponding to the picture
 * if refPalette non NULL, will use the given palette to index image
 * otherwise will create it the palette itself
 * note: max size of the palette is 256 colors */
{
	if (h%8 != 0)
	{
		printf("Erroneous format: input file is not a proper tile set!\n");
		printf("Erroneous format: input file is not a proper tile set!\n");
		exit(1);
	}

	const unsigned int tileW = w/8;
	const unsigned int tileH = h/8;

	struct color {
		u8 r;
		u8 g;
		u8 b;
	} clrPlt[palSize];

	u8 clrCnt = 0;

	if (refPalette != NULL)
	{
		for (clrCnt = 0; clrCnt < palSize; clrCnt++)
		{
			if(clrCnt!=0 && refPalette[clrCnt*3+0] == clrPlt[0].r
			  && refPalette[clrCnt*3+1] == clrPlt[0].g
			  && refPalette[clrCnt*3+2] == clrPlt[0].b)
			{
				break; // If we find a null color not in 0, we stop adding
				// colors to the palette
			}
			clrPlt[clrCnt].r = refPalette[clrCnt*3+0];
			clrPlt[clrCnt].g = refPalette[clrCnt*3+1];
			clrPlt[clrCnt].b = refPalette[clrCnt*3+2];
			turn16Bits(refPalette+(3*clrCnt), gbaPalette+clrCnt);
		}
	}
	unsigned int curPixelIndex = 0;

	for (unsigned int curTileY = 0; curTileY < tileH; curTileY++)
	for (unsigned int curTileX = 0; curTileX < tileW; curTileX++)
	for (unsigned int curInY = 0; curInY < 8; curInY++)
	for (unsigned int curInX = 0; curInX < 8; curInX++, curPixelIndex++)
	{
		unsigned int x = (curTileX*8) + curInX;
		unsigned int y = (curTileY*8) + curInY;
		const u8* curPixel = image + (3*(x + y*w));

		for (u8 curCol = 0; curCol < palSize; curCol++)
		{
			if (clrPlt[curCol].r == curPixel[0] &&
			  clrPlt[curCol].g == curPixel[1] &&
			  clrPlt[curCol].b == curPixel[2]) //color already indexed
			{
				gbaTiles[curPixelIndex] = curCol;
				break;
			}
			else if (curCol >= clrCnt) //we already looked at all teh colors
			{
				clrPlt[curCol].r = curPixel[0];
				clrPlt[curCol].g = curPixel[1];
				clrPlt[curCol].b = curPixel[2];
				turn16Bits(curPixel,gbaPalette+curCol);
				gbaTiles[curPixelIndex] = curCol;
				clrCnt++;
				break;
			}
			//other cases, we continue to look through the palette for the
			//current color
		}
		if (clrCnt == palSize) //after inserting the color, we check if there is
			return clrCnt; //not too many
	}
	return clrCnt;
}

// ------------------ //
// --- assPalette --- //
// ------------------ //
void assPalette(u16* targetPicture, u8* tileSet, u16* pal,
				unsigned int w, unsigned int h)
{
	for (int y = 0; y < h; y++)
		for (int x = 0; x < w; x++)
			targetPicture[x+y*240] = pal[tileSet[x+y*256]];
}
